import { BASE_URL } from "../common/constants";

export const getAllComments = async (postId) => {
  return fetch(`${BASE_URL}/posts/${postId}/comments`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    }
  }).then(response => response.json());
}

export const postComment = async (postId, content) => {
  return fetch(`${BASE_URL}/posts/${postId}/comments`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ content })
  }).then(response => response.json());
}

export const putComment = async (postId, commentId, content) => {
  return fetch(`${BASE_URL}/posts/${postId}/comments/${commentId}`, {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ content })
  }).then(response => response.json());
}

export const deleteComment = async (postId, commentId) => {
  return fetch(`${BASE_URL}/posts/${postId}/comments/${commentId}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },

  }).then(response => response.json());
}
export const getCommentReactions = async (postId, commentId) => {
  return fetch(`${BASE_URL}/posts/${postId}/comments/${commentId}/reactions`, {
    method: 'GET',
    headers: {
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json"
    }
  }).then(response => response.json());
}

export const putCommentReaction = async (postId, commentId, type) => {
  return fetch(`${BASE_URL}/posts/${postId}/comments/${commentId}/reactions`, {
    method: 'PUT',
    headers: {
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ type })
  }).then(response => response.json());
}