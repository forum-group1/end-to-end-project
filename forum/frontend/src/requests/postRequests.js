import { BASE_URL } from "../common/constants";

export const getAllPosts = () => {
  return fetch(`${BASE_URL}/posts`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const getFollowersPosts = () => {
  return fetch(`${BASE_URL}/posts/followers/timeline`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const getPostById = (post_id) => {
  return fetch(`${BASE_URL}/posts/${post_id}`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const getPostsByUsername = (username) => {
  return fetch(`${BASE_URL}/users/${username}/posts`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const createPost = (title, content, users_id) => {
  return fetch(`${BASE_URL}/posts`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      title,
      content,
      users_id,
    }),
  })
    .then((response) => response.json())
};

export const deletePost = async (post_id) => {
  return fetch(`${BASE_URL}/posts/${post_id}`, {
    method: "DELETE",
    headers: {
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": 'application/json',
    },
  }).then((response) => response.json());
};

export const putPostReaction = (post_id, type) => {
  return fetch(`${BASE_URL}/posts/${post_id}/reactions`, {
    method: 'PUT',
    headers: {
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ type })
  }).then(response => response.json());
};

export const getAllPostReactions = (post_id) => {
  return fetch(`${BASE_URL}/posts/${post_id}/reactions`, {
    method: 'GET',
    headers: {
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json"
    }
  }).then(response => response.json());
};

export const updatePost = async (postId, title, content) => {
  return fetch(`${BASE_URL}/posts/${postId}`, {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      title,
      content,
    }),
  }).then(response => response.json());
};

export const flagPost = async (postId) => {
  return fetch(`${BASE_URL}/posts/${postId}/flags`, {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
  }).then(response => response.json());
}