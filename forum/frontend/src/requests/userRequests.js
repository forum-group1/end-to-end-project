import { BASE_URL} from "../common/constants";

export const loginUser = (username, password) => {
  return fetch(`${BASE_URL}/auth/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      username,
      password,
    })
  })
  .then(response => response.json())
};

export const logoutUser = () => {
  return fetch(`${BASE_URL}/auth/logout`, {
    method: 'DELETE',
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
  })
  .then(response => response.json())
  };

export const registerUser = (name, username, password) => {
  return fetch(`${BASE_URL}/users`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      name,
      username,
      password,
    }),
  })
    .then((response) => response.json())
};

export const getAllUsers = () => {
  return fetch(`${BASE_URL}/users`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const getFollowedUsers = (userId) => {
  return fetch(`${BASE_URL}/users/${userId}/friends`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const isFollowing = (friendId) => {
  return fetch(`${BASE_URL}/users/${friendId}/isFriend`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const addFollower = (friendId) => {
  return fetch(`${BASE_URL}/users/${friendId}/friends`, {
    method: "POST",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
}

export const removeFollower = (friendId) => {
  return fetch(`${BASE_URL}/users/${friendId}/friends`, {
    method: "DELETE",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
}

export const getUserByUsername = (username) => {
  return fetch(`${BASE_URL}/users/${username}`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const getUserById = (userId) => {
  return fetch(`${BASE_URL}/users/id/${userId}`, {
    method: "GET",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const banUser = (username) => {
  return fetch(`${BASE_URL}/admin/ban/${username}`, {
    method: "PUT",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};

export const deleteUser = (username) => {
  return fetch(`${BASE_URL}/admin/delete/${username}`, {
    method: "PUT",
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((response) => response.json());
};
