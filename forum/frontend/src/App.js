import './App.css';
import React, { useState } from "react";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from './components/Base/Container/Container';
import Header from './components/Base/Header/Header';
import Main from './components/Base/Main/Main';
import AllPosts from './components/Posts/AllPosts/AllPosts';
import PostDetails from './components/Posts/PostDetails/PostDetails';
import AuthContext from './providers/AuthContext';
import Login from './components/Auth/Login/Login';
import getUserFromToken from './utils/token';
import Register from './components/Auth/Register/Register';
import GuardedRoute from './providers/GuardedRoute';
import { Redirect } from 'react-router';
import AllUsers from './components/Users/AllUsers';
import UserDetails from './components/Users/UserDetails.jsx/UserDetails';
import FollowersPosts from './components/Posts/FollowersPosts';
import FollowedUsers from './components/Users/FollowedUsers';
import UsersPosts from './components/Posts/UsersPosts';
import Landing from './components/Base/Landing/Landing';

function App() {
  const [user, setUser] = useState(getUserFromToken(localStorage.getItem('token')));
  const isUserLogged = !!user;

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      <BrowserRouter>
        <div className="app">
          <Header />
          <Container>
            <Switch>
              <Redirect path="/" exact to="/landing" />
              <Route exact path="/landing" component={Landing} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/home" component={Main} />
              <Route exact path="/users/:username/posts" component={UsersPosts} />
              <Route exact path="/posts/followers/timeline" component={FollowersPosts} />
              <Route exact path="/users" component={AllUsers} />
              <Route exact path="/users/:userId/friends" component={FollowedUsers} />
              <Route exact path="/users/:username" component={UserDetails} />
              <GuardedRoute exact path="/posts" auth={isUserLogged} component={AllPosts} />
              <GuardedRoute exact path="/posts/:id" auth={isUserLogged} component={PostDetails} />
            </Switch>
          </Container>
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default App;

// <Redirect path="/" exact to="/home" />