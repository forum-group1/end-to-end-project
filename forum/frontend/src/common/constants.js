export const userRole = {
  USER: 1,
  ADMIN: 2,
  GUEST: 3,
};

export const BASE_URL = "http://localhost:5000";

export const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywidXNlcm5hbWUiOiJiZW5qYW1pbiIsInJvbGUiOjEsImlhdCI6MTYyNjgwNjQ0OSwiZXhwIjoxNjI3NDExMjQ5fQ.SHkwgGkC5fZ3X1lgOsEbSv-sSNlyfFHtbqCf48Tkk5M";
