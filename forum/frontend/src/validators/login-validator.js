const loginValidator = {
  username: (value) => {
    if (!value) {
      return "Username is required";
    }
    if (value.length < 1 || value.length > 20) {
      return "Username should be 1-20 characters long";
    }
    return null;
  },
  password: (value) => {
    if (!value) {
      return "Password is required";
    }
    if (value.length < 7 || value.length > 20) {
      return "Password should be 7-20 characters long";
    }
    return null;
  },
};

export default loginValidator;
