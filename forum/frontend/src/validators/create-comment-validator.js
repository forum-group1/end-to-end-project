const createCommentValidator = {
  content: (value) => {
    if (!value) {
      return 'Content is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 5000) {
      return 'Content should be 1-5000 characters long';
    }
    return null;
  },
};

export default createCommentValidator;