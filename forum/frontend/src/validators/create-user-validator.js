const createUserValidator = {
  name: (value) => {
    if (!value) {
      return 'Name is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 100) {
      return 'Name should be 1-100 characters long';
    }
    return null;
  },
  username: (value) => {
    if (!value) {
      return 'Username is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 50) {
      return 'Username should be 1-50 characters long';
    }
    return null;
  },
  password: (value) => {
    if (!value) {
      return 'Password is required';
    }
    if (typeof value !== 'string' || value.length < 7 || value.length > 50) {
      return 'Password should be 7-50 characters long';
    }
    return null;
  },
};

export default createUserValidator;