const updateUserValidator = {
  name: (value) => {
    if (!value) {
      return null;
    }
    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return 'Name should be 3-25 characters long';
    }
    return null;
  },
};

export default updateUserValidator;