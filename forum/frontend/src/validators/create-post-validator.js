const createPostValidator = {
  title: (value) => {
    if (!value) {
      return 'Title is required';
    }
    if (value.length < 1 || value.length > 70) {
      return 'Title should be a 1-70 characters long';
    }
    return null;
  },
  content: (value) => {
    if (!value) {
      return 'Content is required';
    }
    if (value.length < 1 || value.length > 255) {
      return 'Content should be 1-255 characters long';
    }
    return null;
  },
};

export default createPostValidator;