export const convertFromUTCTime = (utcString) => {
    const date = new Date(utcString);
    // const time = date.getTime();
    return date.toLocaleDateString();
}