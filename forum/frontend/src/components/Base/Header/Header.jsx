import React, { useContext, useEffect } from "react";
import AuthContext from "../../../providers/AuthContext";
import { Button } from "react-bootstrap";
import { useHistory, Link } from "react-router-dom";
import { logoutUser } from "../../../requests/userRequests";
import "./Header.css";
import logo from "../../../common/images/whitelogo.png"

const Header = () => {
  const { user, setUser } = useContext(AuthContext);
  let history = useHistory();

  const triggerLogout = () => {
    logoutUser()
      .then(response => {
        history.push("/");
        setUser(null);
        localStorage.removeItem('token');
      });
  }

  useEffect(() => {
    if (!user) return; // no need to logout a user if they are already logged out
    const timer = setTimeout(() => {
      logoutUser()
      history.push("/");
      setUser(null);
      localStorage.removeItem('token');
    }, new Date(user?.exp * 1000 || 0) - new Date()); // multiply by 1000 to get seconds 

    return () => clearTimeout(timer);
  }, [user, history, setUser]);

  const showHome = () => {
    history.push('/home');
  };

  const showAllPosts = () => {
    history.push('/posts');
  };

  const showMyPosts = () => {
    history.push(`/users/${user.username}/posts`);
  };

  const showFollowersPosts = () => {
    history.push('/posts/followers/timeline');
  };

  const showAllUsers = () => {
    history.push('/users');
  };

  const showFriends = () => {
    history.push(`/users/${user.id}/friends`);
  };

  const login = () => {
    history.push('/login');
  };

  const register = () => {
    history.push('/register');
  }

  return (
    <>
      {!!user ?
        <div className="forum-header-bar">
          <img src={logo} height="45px" alt="Forum Logo" onClick={showHome} />
          <div className="user-data">
            <Button className="logout-button" variant="light" onClick={showAllPosts}>All Posts</Button>
            <Button className="logout-button" variant="light" onClick={showMyPosts}>My Posts</Button>
            <Button className="logout-button" variant="light" onClick={showFollowersPosts}>Friends' Posts</Button>
            <Button className="logout-button" variant="light" onClick={showAllUsers}>Users</Button>
            <Button className="logout-button" variant="light" onClick={showFriends}>Friends</Button>
            <div className="user-text-container">
              <p className="user-text"> Signed in as:</p>
              <Link to={`/users/${user.username}`}>
                <p className="user-text-bold">{user.username}</p>
              </Link>
            </div>
            <Button className="logout-button" variant="danger" onClick={triggerLogout}>Logout</Button>
          </div>
        </div> :
        <div className="forum-header-bar">
          <img src={logo} height="45px" alt="Forum Logo" />
          <div className="authentication">
            <Button className="logout-button" variant="light" onClick={login}>Login</Button>
            <Button className="logout-button" variant="light" onClick={register}>Register</Button>
          </div>
        </div>
      }
    </>
  );
};

export default Header;