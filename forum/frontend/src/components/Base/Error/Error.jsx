import React from "react";
import error from "../../../common/images/icons/error.png";
import './Error.css';
import PropTypes from 'prop-types';

const Error = ({ message }) => {

  return (
    <div className="error-container">
      <div className="inner-container">
        <img src={error} wideth="40px" height="40px" alt="error icon" />
        <p className="error-text">{message}</p>
      </div>
    </div >
  );
};

Error.propTypes = {
  message: PropTypes.string
}

export default Error;
