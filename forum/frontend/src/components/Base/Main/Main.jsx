import React from "react";
import CreatePost from "../../Posts/CreatePost/CreatePost";
import "./Main.css"

const Main = () => {

  return (
    <div className="main-container">
      <h3>Home Page</h3>
      <br/>
      <CreatePost/>
    </div>
    );
};

export default Main;
