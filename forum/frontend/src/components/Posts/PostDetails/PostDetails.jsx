import React, { useState, useEffect } from "react";
import { getPostById, deletePost, flagPost } from "../../../requests/postRequests";
// import AllComments from "../../Comments/AllComments/AllComments";
import SinglePost from "../SinglePost/SinglePost";
import Error from "../../Base/Error/Error";
import './PostDetails.css';
import { Spinner } from "react-bootstrap";
import PropTypes from 'prop-types';

const PostDetails = (props) => {
  const id = props.match.params.id;
  const [postDetails, setPostDetails] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    getPostById(id)
    .then(response => {
      if (response.error) {
        setError(response.error);
      } else {
        setPostDetails(response.data)
      }
    });
  }, [id]);

  const handleDeletePost = (postId) => {
    deletePost(postId)
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          if (result.data.singlePost === null) {
            setPostDetails([]);
            setError("Post not found!");
          }
          setPostDetails(result.data.singlePost);
        }
      });
  };

  const handleFlagPost = (postId) => {
    flagPost(postId)
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          if (result.data.allPosts === null) {
            setPostDetails([]);
          }
          setPostDetails(result.data.singlePost);
        }
      });
  };

  if (!postDetails && error===null) {
    return <Spinner animation="border"/>
  }

  if (error) {
    return <Error message={error}/>
  }

  
  return (
    <div className="post-container">

    {postDetails
      ? <SinglePost key={id} post={postDetails} handleDeletePost={handleDeletePost} handleFlagPost={handleFlagPost} />
      : null
    }

    </div>
  );
};

PostDetails.propTypes = {
  id: PropTypes.number
}
export default PostDetails;
