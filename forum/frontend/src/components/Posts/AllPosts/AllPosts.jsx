import React, { useState, useEffect } from "react";
import { deletePost, flagPost, getAllPosts } from "../../../requests/postRequests";
import SinglePost from "../SinglePost/SinglePost";
import Error from "../../Base/Error/Error";
import './AllPosts.css';
import { Spinner } from "react-bootstrap";

const AllPosts = () => {
  const [allPosts, setAllPosts] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    getAllPosts()
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          setAllPosts(result.data)
        }
      })
  }, []);

  const handleDeletePost = (postId) => {
    deletePost(postId)
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          if (result.data.allPosts === null) {
            setAllPosts([]);
            setError("Posts not found!");
          }
          setAllPosts(result.data.allPosts);
        }
      });
  };

  const handleFlagPost = (postId) => {
    flagPost(postId)
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          if (result.data.allPosts === null) {
            setAllPosts([]);
          }
          setAllPosts(result.data.allPosts);
        }
      });
  };




  if (!allPosts && error === null) {
    return <Spinner animation="border" />
  }

  if (error) {
    return <Error message={error} />
  }

  return (
    <div className="posts-list">
      {allPosts?.map((post) => (<SinglePost key={post.id} post={post} handleDeletePost={handleDeletePost} handleFlagPost={handleFlagPost} />))}
    </div>
  );
};

export default AllPosts;
