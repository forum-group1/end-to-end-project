import React, { useState, useContext } from "react";
import AuthContext from "../../../providers/AuthContext";
import { Button, Form } from "react-bootstrap";
import { createPost } from "../../../requests/postRequests";
import { useHistory } from "react-router-dom";
import createPostValidator from "../../../validators/create-post-validator";
import Error from "../../Base/Error/Error";
import './CreatePost.css';

const CreatePost = () => {
  const { user } = useContext(AuthContext);
  const history = useHistory();

  const [alert, setAlert] = useState(null);
  const [isFormValid, setIsFormValid] = useState(false);
  const [post, setPost] = useState({
      title: {
        type: "text",
        value: '',
        placeholder: "",
        validityFeedback: '',
      },
      content: {
        type: "text",
        value: '',
        placeholder: "what's on your mind?",
        validityFeedback: '',
      }
    }
  );

  const handleInputChange = (event) => {
    const {name, value} = event.target;
    const updatedInput = post[name];

    updatedInput.value = value;
    updatedInput.validityFeedback = createPostValidator[name](value);

    setPost({...post, [name]: updatedInput});
    const formValid = Object.values(post).every(property => property.validityFeedback===null);
    setIsFormValid(formValid);
  };

  const triggerCreatePost = (event) => {
    event.preventDefault();

    createPost(post.title.value, post.content.value, user.id)
    .then(response => {
      if (response.error) {
        setAlert(response.error);
      } else {
        history.push(`/posts/${response.data.id}`);
      }
    })
  };

  const inputFields = Object.entries(post).map(([key, element]) => {
    return (
      <Form.Group className="mb-3" key={key}>
        <Form.Label>{key}</Form.Label>
        <Form.Control
          name={key} 
          type={element.type} 
          value={element.value}
          onChange={handleInputChange} 
          placeholder={element.placeholder} 
        />
        <Form.Text>
          {element.validityFeedback}
        </Form.Text>
      </Form.Group>
    )
  });

  return (
    <div className="create-post-container">
      <h5>Create a new post</h5>
      <Form onSubmit={(event) => triggerCreatePost(event)}>
        {inputFields}
        <Button type="submit" variant="dark" disabled={!isFormValid}>Submit</Button>
      </Form>

      {alert
        ? <Error message={alert}></Error>
        : null
      }
    </div>
  )
};

export default CreatePost;
