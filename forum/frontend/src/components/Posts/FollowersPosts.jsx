import React, { useState, useEffect } from "react";
import { getFollowersPosts, deletePost, flagPost } from "../../requests/postRequests";
import Error from "../Base/Error/Error";
import SinglePost from "./SinglePost/SinglePost";
import { Spinner } from "react-bootstrap";

const FollowersPosts = () => {
  const [followersPosts, setFollowersPosts] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    getFollowersPosts()
      .then(response => {
        if (response.error) {
          setError(response.error);
        } else {
          setFollowersPosts(response.data)
        }
      })
  }, []);

  const handleDeletePost = (postId) => {
    deletePost(postId)
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          if (result.data.followersPosts === null) {
            setFollowersPosts([]);
            setError("Posts not found!");
          }
          setFollowersPosts(result.data.followersPosts);;
        }
      });
  };

  const handleFlagPost = (postId) => {
    flagPost(postId)
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          if (result.data.allPosts === null) {
            setFollowersPosts([]);
          }
          setFollowersPosts(result.data.followersPosts);
        }
      });
  };


  if (!FollowersPosts && error === null) {
    return <Spinner animation="border" />
  }

  if (error) {
    return <Error message={error} />
  }

  return (
    <div className="posts-list">
      {followersPosts?.map((post) => (<SinglePost key={post.id} post={post} handleDeletePost={handleDeletePost} handleFlagPost={handleFlagPost} />))}
    </div>
  );
};

export default FollowersPosts;
