import React, { useState, useEffect } from "react";
import { deletePost, flagPost, getPostsByUsername } from "../../requests/postRequests";
import SinglePost from "./SinglePost/SinglePost";
import Error from "../Base/Error/Error";
import PropTypes from 'prop-types';

const UsersPosts = (props) => {
  const username = props.match.params.username;

  const [usersPosts, setUsersPosts] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    getPostsByUsername(username)
      .then(response => {
        if (response.error) {
          setError(response.error);
        } else {
          setUsersPosts(response.data);
        }
      })
  }, [username]);

  const handleDeletePost = (postId) => {
    deletePost(postId)
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          if (result.data.userPosts === null) {
            setUsersPosts([]);
            setError("Posts not found!");
          }
          setUsersPosts(result.data.userPosts);
        }
      });
  };

  const handleFlagPost = (postId) => {
    flagPost(postId)
      .then(result => {
        if (result.error) {
          setError(result.error);
        } else {
          if (result.data.userPosts === null) {
            setUsersPosts([]);
          }
          setUsersPosts(result.data.userPosts);
        }
      });
  };


  if (error) {
    return <Error message={error} />
  }


  return (
    <div className="posts-list">
      {usersPosts?.map((post) => (<SinglePost key={post.id} post={post} handleDeletePost={handleDeletePost} handleFlagPost={handleFlagPost} />))}
    </div>
  );
};

UsersPosts.propTypes = {
  username: PropTypes.string
}
export default UsersPosts;
