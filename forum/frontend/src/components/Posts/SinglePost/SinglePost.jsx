import React, { useState } from "react";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import AllComments from "../../Comments/AllComments/AllComments";
import { getAllPostReactions, updatePost } from "../../../requests/postRequests";
import PostReaction from "../../Reactions/PostReactions/PostReactions";
import expand from "../../../common/images/icons/expand.png";
import collapse from "../../../common/images/icons/collapse.png";
import comment from "../../../common/images/icons/comment.png";
import userIcon from "../../../common/images/user.jpg";
import saveIcon from '../../../common/images/icons/save.png';
import editIcon from "../../../common/images/icons/edit.png";
import deleteIcon from "../../../common/images/icons/delete.png";
import closeIcon from "../../../common/images/icons/close.png";
import flagIcon from "../../../common/images/icons/flag.png";
import CreateComment from "../../Comments/AllComments/CreateComment/CreateComment";
import SingleComment from "../../Comments/SingleComment/SingleComment";
import Error from "../../Base/Error/Error";
import PropTypes from 'prop-types';
import "./SinglePost.css";
import { useContext } from "react";
import AuthContext from "../../../providers/AuthContext";

const SinglePost = ({ post, handleDeletePost, handleFlagPost }) => {
  const { user } = useContext(AuthContext);
  const error = (() => {
    if (post.is_locked === 1 && user.role !== 2) {
      return true;
    }
    return false;
  })();

  const isPostMine = (user.id === post.users_id) || (user.role === 2);

  const history = useHistory();
  const [showComments, setShowComments] = useState(false);
  const [inEditMode, setInEditMode] = useState(false);
  const [postTitle, setPostTitle] = useState(post.title);
  const [postContent, setPostContent] = useState(post.content);
  const [errorMessage, setErrorMessage] = useState(null);
  const [comments, setComments] = useState([]);
  const [addingComment, setAddingComment] = useState(false);
  const [postTitleMemo, setPostTitleMemo] = useState('');
  const [postContentMemo, setPostContentMemo] = useState('');

  const edit = () => {
    if (!error) {
      setInEditMode(true);
      setPostTitleMemo(postTitle);
      setPostContentMemo(postContent);
    }
    else {
      setErrorMessage("Post locked!");
    }
  }

  const closeEdit = () => {
    setInEditMode(false);
    updatePost(post.id, postTitle, postContent)
      .then(response => {
        if (response.error) {
          setErrorMessage(response.error);
        }
      });
  }

  const changeTitleInput = (e) => {
    setPostTitle(e.target.value);
  }

  const changeContentInput = (e) => {
    setPostContent(e.target.value);
  }

  const toggleComments = () => {
    setShowComments(!showComments);
  }

  const showSinglePost = () => {
    history.push(`posts/${post.id}`);
  }


  const addComment = () => {
    setShowComments(true);
    setAddingComment(true);
  }

  const closeCommentEdit = () => {
    setInEditMode(false);
    setAddingComment(false);
  }

  const closePostEdit = () => {
    setPostContent(postContentMemo);
    setPostTitle(postTitleMemo);
    setInEditMode(false);
    setAddingComment(false);
  }

  const handleComment = (comment) => {
    setAddingComment(false);
    setComments([...comments, comment]);
  }

  return (

    <div className="single-post-container">

      <div className="user-info-header">
        <div className="user-information">
          <img src={userIcon} className="user-icon" alt="user logo" width="50px" height="50px" />
          <Link className="link" to={`/users/${post.username}`}>
            <div className="username"> {post.name}</div>
          </Link>
        </div>
        {user.isBanned ? null : <div className="control-icons">
          {inEditMode ?
            <div className="close-save-controls">
              <img src={saveIcon} alt="edit-icon" className='edit-icon' width="30px" height="30px" onClick={closeEdit} />
              <img src={closeIcon} alt="close-icon" className="delete-icon" width="30px" height="30px" onClick={closePostEdit} />
            </div>
            : <div>
              {isPostMine ?
                <>
                  <img src={editIcon} alt="edit-icon" className='edit-icon' width="30px" height="30px" onClick={edit} />
                  <img src={deleteIcon} alt="delete-icon" className='delete-icon' width="30px" height="30px" onClick={() => handleDeletePost(post.id)} />
                </> : null}
              <img src={flagIcon} alt="delete-icon" className={(post.is_flagged === 1) ? 'flagged' : 'delete-icon'} width="30px" height="30px" onClick={() => handleFlagPost(post.id)} />
            </div>
          }
        </div>
        }
      </div >

      <div className="post-details">
        {errorMessage ? <Error message={errorMessage} /> : null}

        {inEditMode
          ? <input className="post-title" type="text" placeholder={post.title} value={postTitle} onChange={changeTitleInput} autoFocus />
          : <Link className="link" to={`/posts/${post.id}`}>
            <p className="title" onClick={showSinglePost}> {postTitle}</p>
          </Link>
        }
        {inEditMode
          ? <input className="post-content" type="text" placeholder={post.content} value={postContent} onChange={changeContentInput} autoFocus />
          : <div className="post-content"> {postContent}</div>
        }


        <div className="reactions"> </div>

        {showComments
          ?
          <div className="expanded">
            <div className="icon-controls" >
              <div className="icon-text" onClick={toggleComments}>
                <img src={collapse} alt="collapse icon" width="30px" height="30px" />
                <p className="nav-text"> Collapse comments </p>
              </div>
              {user.isBanned ? null :
                <>
                  <PostReaction id={post.id} handlerFunction={getAllPostReactions} />
                  <div className="icon-text" onClick={addComment}>
                    <img src={comment} className={"comment-icon"} alt="comment icon" width="30px" height="30px" />
                    <p className="nav-text"> Add comment </p>
                  </div>
                </>
              }
            </div>

            <div className="comment-section">
              {addingComment ? <CreateComment post={post} commentHandler={handleComment} closeEdit={closeCommentEdit} /> : null}
              <AllComments post={post} />
              {comments.map(comment => <SingleComment post={post} comment={comment} />)}
            </div>
          </div>
          :
          <div className="collapsed">

            <div className="icon-controls" >
              <div className="icon-text" onClick={toggleComments}>
                <img src={expand} alt="expand icon" width="30px" height="30px" />
                <p className="nav-text"> Expand comments </p>
              </div>
              {user.isBanned ? null :
                <>
                  <PostReaction id={post.id} handlerFunction={getAllPostReactions} />
                  <div className="icon-text" onClick={addComment}>
                    <img src={comment} className={"comment-icon"} alt="comment icon" width="30px" height="30px" />
                    <p className="nav-text"> Add comment </p>
                  </div>
                </>
              }
            </div>
          </div>
        }

      </div>
    </div >
  );
};

CreateComment.propTypes = {
  post: PropTypes.object
}
export default SinglePost;
