import React from 'react';
import loading from "../../../common/images/cat-loading.gif";
import './Loading.css';

const Loading = () => {
  return (
    <img src={loading} className="loading" width="200px" height="200px" alt="loading indicator" />
  )
}

export default Loading;