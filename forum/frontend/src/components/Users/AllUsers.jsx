import React, { useState, useEffect } from "react";
import { getAllUsers } from "../../requests/userRequests";
import SingleUser from "./SingleUser/SingleUser";
import Error from "../Base/Error/Error";
import { Spinner } from "react-bootstrap";

const AllUsers = () => {
  const [allUsers, setAllUsers] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    getAllUsers()
      .then(response => {
        if (response.error) {
          setError(response.error);
        } else {
          setAllUsers(response.data)
        }
      })
  }, []);

  if (!allUsers && error===null) {
    return <Spinner animation="border"/>;
  }

  if (error) {
    return <Error message={error}/>
  }

  return (
    <div className="users-list">
      {allUsers?.map((user) => (
        <SingleUser key={user.id} user={user} />
      ))}      
    </div>
  );
};

export default AllUsers;
