import React, { useState, useEffect, useContext } from "react";
import AuthContext from "../../../providers/AuthContext";
import SingleUser from "../SingleUser/SingleUser";
import { getUserByUsername, deleteUser, banUser } from "../../../requests/userRequests";
import { userRole } from "../../../common/constants";
import { Button, Spinner } from "react-bootstrap";
import Error from "../../Base/Error/Error";
import "./UserDetails.css"
import { convertFromUTCTime } from "../../../utils/dates";
import PropTypes from 'prop-types';

const UserDetails = (props) => {
  const username = props.match.params.username;

  const [userDetails, setUserDetails] = useState(null);
  const [userDeleted, setUserDeleted] = useState(false);
  const [userBanned, setUserBanned] = useState(false);
  const [error, setError] = useState(null);

  const { user, } = useContext(AuthContext);

  useEffect(() => {
    getUserByUsername(username)
      .then(response => {
        if (response.error) {
          setError(response.error);
        } else {
          setUserDetails(response.data);
          setUserBanned(response.data.banned_until);
        }
      })

  }, [username]);


  const triggerDeleteUser = () => {
    deleteUser(username)
      .then(response => {
        if (response.error) {
          setError(response.error);
        } else {
          setUserDeleted(true);
        }
      })
  }

  const triggerBanUser = () => {
    banUser(username)
      .then(response => {
        if (response.error) {
          setError(response.error);
        } else {
          setUserBanned(response.data[0].banned_until);
        }
      })
  }

  if (!userDetails && error === null) {
    return <Spinner animation="border" />
  }

  if (error) {
    return <Error message={error} />
  }

  return (
    <div className="user-details-container">

      <SingleUser key={username} user={userDetails} />

      <br />

      <div className="admin-controls">
        {(user.role === userRole.ADMIN)
          ? <Button className="delete-user-button" variant="dark" onClick={() => triggerDeleteUser(username)}>Delete User</Button>
          : null
        }

        {(user.role === userRole.ADMIN)
          ? <Button className="ban-user-button" variant="dark" onClick={() => triggerBanUser(username)}>Ban User</Button>
          : null
        }
      </div>

      <br />

      <div className="admin-messages">
        {((user.role === userRole.ADMIN) && (userDeleted))
          ? <p>User Deleted</p>
          : null
        }
        <br />
        {((user.role === userRole.ADMIN) && (userBanned))
          ? <p>User banned until: {convertFromUTCTime(userBanned)}</p>
          : null
        }
      </div>

    </div>
  )
};

UserDetails.propTypes = {
  username: PropTypes.string
}
export default UserDetails;


