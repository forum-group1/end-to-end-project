import React, { useState, useEffect } from "react";
import { getFollowedUsers } from "../../requests/userRequests";
import SingleUser from "./SingleUser/SingleUser";
import Error from "../Base/Error/Error";
import { Spinner } from "react-bootstrap";
import PropTypes from 'prop-types';

const FollowedUsers = (props) => {
  const userId = props.match.params.userId;

  const [followedUsers, setFollowedUsers] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    getFollowedUsers(userId)
      .then(response => {
        if (response.error) {
          setError(response.error);
        } else {
          setFollowedUsers(response.data);
        }
      })
  }, [userId]);

  if (!followedUsers && error === null) {
    return <Spinner animation="border" />
  }

  if (error) {
    return <Error message={error} />
  }

  return (
    <div className="users-list">
      {followedUsers?.map((user) => (<SingleUser key={user.id} user={user} />))}
    </div>
  );
};

FollowedUsers.propTypes = {
  userId: PropTypes.number
}
export default FollowedUsers;
