import React, { useContext, useEffect, useState } from "react";
import userImage from "../../../common/images/user.jpg";
import { Link } from "react-router-dom";
import addFriendIcon from "../../../common/images/icons/addFriend.png";
import removeFriendIcon from "../../../common/images/icons/removeFriend.png";
import { addFollower, isFollowing, removeFollower } from "../../../requests/userRequests";
import AuthContext from "../../../providers/AuthContext";
import "./SingleUser.css";

const SingleUser = ({ user }) => {
  const me = useContext(AuthContext);
  const [isFriend, setIsFriend] = useState(false);
  console.log(me);

  useEffect(() => {
    isFollowing(user.id)
      .then(response => {
        if (response.data === true) {
          setIsFriend(true);
        } else {
          setIsFriend(false);
        }
      }).catch(error => alert(error));
  }, [me.id, user.id]);

  const triggerAddFriend = (userId) => {
    addFollower(userId)
      .then(response => {
        if (response.data === true) {
          setIsFriend(true);
        }
      });
  };

  const triggerRemoveFriend = (userId) => {
    removeFollower(userId)
      .then(response => {
        if (response.data === true) {
          setIsFriend(false);
        }
      });
  };

  return (
    <div className="single-user-container">

      <div className="profile-picture">
        <img src={userImage} className="user-icon" alt="user logo" width="50px" height="50px" />
      </div>

      <div className="single-user-info">
        <Link className="link" to={`/users/${user.username}`}>
          <p>Name: {user.name}</p>
        </Link>
        <p>Username: {user.username}</p>
      </div>

      {(user.id === me.user.id || me.user.isBanned)
        ? null
        : <div className="friend-buttons">
          {isFriend
            ? <img src={removeFriendIcon} onClick={() => { triggerRemoveFriend(user.id); window.location.reload(false) }} className="unfollow-icon" alt="user logo" width="30px" height="30px" />
            : <img src={addFriendIcon} onClick={() => triggerAddFriend(user.id)} className="follow-icon" alt="user logo" width="30px" height="30px" />
          }
        </div>
      }

    </div>
  );
};

export default SingleUser;

// {isFriend ? <p>Following</p> : <p>Not Following</p>}