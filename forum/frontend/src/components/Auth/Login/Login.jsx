import React, { useContext, useState } from "react";
import AuthContext from "../../../providers/AuthContext";
import getUserFromToken from "../../../utils/token";
import { Button, Form } from "react-bootstrap";
import { loginUser } from "../../../requests/userRequests";
import { useHistory } from "react-router";
import loginValidator from "../../../validators/login-validator";
import Error from "../../Base/Error/Error";
import "./Login.css";

const Login = () => {
  const { setUser } = useContext(AuthContext);
  const history = useHistory();

  const [alert, setAlert] = useState(null);
  const [isFormValid, setIsFormValid] = useState(false);
  const [credentials, setCredentials] = useState({
      username: {
        type: "text",
        value: '',
        placeholder: "",
        validityFeedback: '',
      },
      password: {
        type: "password",
        value: '',
        placeholder: "",
        validityFeedback: '',
      }
    }
  );

  const handleInputChange = (event) => {
    const {name, value} = event.target;
    const updatedInput = credentials[name];

    updatedInput.value = value;
    updatedInput.validityFeedback = loginValidator[name](value);

    setCredentials({...credentials, [name]: updatedInput});
    const formValid = Object.values(credentials).every(property => property.validityFeedback===null);
    setIsFormValid(formValid);
  };

  const triggerLogin = (event) => {
    event.preventDefault();

    loginUser(credentials.username.value, credentials.password.value)
    .then(response => {
      if (response.token) {
        setUser(getUserFromToken(response.token));
        localStorage.setItem('token', response.token);
        history.push('/home');
      } else {
        setAlert(response.message);
      }
    });
  };

  const inputFields = Object.entries(credentials).map(([key, element]) => {
    return (
      <Form.Group className="mb-3" key={key}>
        <Form.Label>{key}</Form.Label>
        <Form.Control
          name={key} 
          type={element.type} 
          value={element.value}
          onChange={handleInputChange} 
          placeholder={element.placeholder} 
        />
        <Form.Text>
          {element.validityFeedback}
        </Form.Text>
        <br/>
      </Form.Group>
    )
  });

  return (
    <div className="login-container">
      <h2>Login</h2>
      <br/>
      <Form onSubmit={(event) => triggerLogin(event)}>
        {inputFields}
        <Button type="submit" variant="dark" disabled={!isFormValid}>Submit</Button>
      </Form>

      <br/>

      {alert
        ? <Error message={alert}></Error>
        : null
      }
    </div>
  );
};

export default Login;
