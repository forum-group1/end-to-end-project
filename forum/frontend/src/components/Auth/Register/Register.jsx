import React, { useState, useContext } from "react";
import { Button, Form } from "react-bootstrap";
import AuthContext from "../../../providers/AuthContext";
import getUserFromToken from "../../../utils/token";
import { registerUser, loginUser } from "../../../requests/userRequests";
import createUserValidator from "../../../validators/create-user-validator";
import Error from "../../Base/Error/Error";
import { useHistory } from "react-router-dom";
import "./Register.css";

const Register = () => {
  const { setUser } = useContext(AuthContext);
  const history = useHistory();

  const [alert, setAlert] = useState(null);
  const [isFormValid, setIsFormValid] = useState(false);
  const [credentials, setCredentials] = useState({
    name: {
      type: "text",
      value: '',
      placeholder: "",
      validityFeedback: '',
    },
    username: {
      type: "text",
      value: '',
      placeholder: "",
      validityFeedback: '',
    },
    password: {
      type: "password",
      value: '',
      placeholder: "",
      validityFeedback: '',
    }
  });

  const handleInputChange = (event) => {
    const {name, value} = event.target;
    const updatedInput = credentials[name];

    updatedInput.value = value;
    updatedInput.validityFeedback = createUserValidator[name](value);

    setCredentials({...credentials, [name]: updatedInput});
    const formValid = Object.values(credentials).every(property => property.validityFeedback===null);
    setIsFormValid(formValid);
  };

  const triggerRegister = (event) => {
    event.preventDefault();
    registerUser(credentials.name.value, credentials.username.value, credentials.password.value)
    .then(response => {
      if (response.error) {
        setAlert(response.error);
      } else {
        loginUser(credentials.username.value, credentials.password.value) // login the user
        .then(response => {
          if (response.token) {
            setUser(getUserFromToken(response.token));
            localStorage.setItem('token', response.token);
            history.push('/home');
          } else {
            setAlert(response.message);
          }
        })
      }
    });
  };

  const inputFields = Object.entries(credentials).map(([key, element]) => {
    return (
      <Form.Group className="mb-3" key={key}>
        <Form.Label>{key}</Form.Label>
        <Form.Control
          name={key} 
          type={element.type} 
          value={element.value}
          onChange={handleInputChange} 
          placeholder={element.placeholder} 
        />
        <Form.Text>
          {element.validityFeedback}
        </Form.Text>
        <br/>
      </Form.Group>

    )
  });

  return (
    <div className="register-container">
      <h2>Register</h2>
      <br/>
      <Form onSubmit={(event) => triggerRegister(event)}>
        {inputFields}
        <Button type="submit" variant="dark" disabled={!isFormValid}>Submit</Button>
      </Form>
      
      <br/>

      {alert
        ? <Error message={alert}></Error>
        : null
      }
    </div>
  );
};

export default Register;
