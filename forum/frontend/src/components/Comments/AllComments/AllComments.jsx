import React, { useState, useEffect } from 'react';
import { deleteComment, getAllComments } from '../../../requests/commentRequests';
import Error from '../../Base/Error/Error';
import SingleComment from '../SingleComment/SingleComment';
import './AllComments.css';
import PropTypes from 'prop-types';


const AllComments = ({ post }) => {
  const [comments, setComments] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    getAllComments(post.id)
      .then(result => {
        if (result.error) {
          throw new Error(result.error)
        }
        setComments(result.data);
      }).catch(err => setError(err));
  }, [post.id]);

  if (error) {
    return (
      <Error message={error} />)
  }

  const handleComments = (postId, commentId) => {
    deleteComment(postId, commentId)
      .then(result => {
        if (result.error) {
          throw new Error(result.error)
        }
        setComments(result.data);
      }).catch(err => setError(err.message));
  };

  return (
    <div className="comments-container">
      {comments.map(comment => <SingleComment key={comment.id} comment={comment} post={post} username={post.name} handleComments={handleComments} />)}
    </div>
  )
};

AllComments.propTypes = {
  post: PropTypes.object
}
export default AllComments;
