import React, { useContext } from 'react';
import userIcon from '../../../../common/images/user.jpg';
import saveIcon from '../../../../common/images/icons/save.png';
import { useState } from 'react';
import Error from '../../../Base/Error/Error';
import { postComment } from '../../../../requests/commentRequests';
import AuthContext from '../../../../providers/AuthContext';
import PropTypes from 'prop-types';
import closeIcon from '../../../../common/images/icons/close.png'

const CreateComment = ({ post, commentHandler, closeEdit }) => {

  const err = (() => {
    if (post.is_locked === 1) {
      return true;
    }
    return false;
  })();

  const { user } = useContext(AuthContext);

  const [error, setError] = useState(err);
  const [content, setContent] = useState('');

  const changeInput = (e) => {
    setContent(e.target.value)
  }

  const saveAndReload = () => {
    postComment(post.id, content)
      .then(result => {
        if (result.error) {
          alert('Error Maina!')
          throw new Error(result.error);
        }
        commentHandler(result.data);
      }).catch(err => alert(err));
  }

  if (error) {
    return <Error message="Post is locked and cannot be commented on" />
  }

  return (
    <div className="single-comment-container">
      <div className="comment-header">
        <div className="user-info">
          <img src={userIcon} alt="profile-pic" className='profile-pic' width="30px" height="30px" />
          <p className="name">{user.name}</p>
        </div>
        <div className="control-icons">
          <img src={saveIcon} alt="edit-icon" className='delete-icon' width="30px" height="30px" onClick={saveAndReload} />
          <img src={closeIcon} alt="close-icon" className="delete-icon" width="30px" height="30px" onClick={closeEdit} />
        </div>
      </div>
      <input className="comment-content" type="text" placeholder="Enter your comment here" value={content} onChange={changeInput} autoFocus />
    </div>
  )
}

CreateComment.propTypes = {
  post: PropTypes.object,
  commentHandler: PropTypes.func
}
export default CreateComment;