import React, { useState } from 'react';
import CommentReactions from '../../Reactions/CommentReactions/CommentReactions';
import Error from '../../Base/Error/Error';
import userIcon from '../../../common/images/user.jpg';
import deleteIcon from '../../../common/images/icons/delete.png';
import editIcon from '../../../common/images/icons/edit.png';
import saveIcon from '../../../common/images/icons/save.png';
import closeIcon from '../../../common/images/icons/close.png';
import './SingleComment.css';
import { putComment } from '../../../requests/commentRequests';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import AuthContext from '../../../providers/AuthContext';

const SingleComment = ({ comment, post, handleComments }) => {


  const error = (() => {
    if (post.is_locked === 1) {
      return true;
    }
    return false;
  })();

  const [inEditMode, setInEditMode] = useState(false);
  const [commentContent, setCommentContent] = useState(comment.content);
  const [renderError, setRenderError] = useState(false);
  const [commentContentMemo, setCommentContentMemo] = useState('');

  const { user } = useContext(AuthContext);

  const isEdittable = ((user.id === comment.users_id) && (!user.isBanned)) || (user.role === 2);

  const edit = () => {
    if (!error) {
      setInEditMode(true);
      setCommentContentMemo(commentContent);
    }
    else {
      setRenderError(true);
    }
  }

  const closeEdit = () => {
    setInEditMode(false);
    putComment(post.id, comment.id, commentContent)
      .then(result => {
        if (result.error) {
          throw new Error(result.error);
        }
      }).catch(e => alert('e.message'));
  }

  const changeInput = (e) => {
    setCommentContent(e.target.value);
  }

  const resetCommentState = () => {
    setInEditMode(false);
    setCommentContent(commentContentMemo);
  }
  return (
    <div className="single-comment-container">
      <div className="comment-header">
        <div className="user-info">
          <img src={userIcon} alt="profile-pic" className='profile-pic' width="30px" height="30px" />
          <p className="name">{comment.name}</p>
        </div>
        <div className="control-icons">
          {inEditMode ?
            <>
              <img src={saveIcon} alt="edit-icon" className='edit-icon' width="30px" height="30px" onClick={closeEdit} />
              <img src={closeIcon} alt="edit-icon" className='edit-icon' width="30px" height="30px" onClick={resetCommentState} />
            </>
            : <div>
              {isEdittable ?
                <>
                  <img src={editIcon} alt="edit-icon" className='edit-icon' width="30px" height="30px" onClick={edit} />
                  <img src={deleteIcon} alt="delete-icon" className='delete-icon' width="30px" height="30px" onClick={() => handleComments(post.id, comment.id)} />
                </>
                : null}
            </div>
          }
        </div>
      </div>
      {renderError ? <Error message="Post locked!" /> : null}
      {inEditMode
        ? <input className="comment-content" type="text" placeholder={comment.content} value={commentContent} onChange={changeInput} autoFocus />
        : <p className="comment-content" onDoubleClick={edit}>{commentContent}</p>}
      <CommentReactions postId={post.id} commentId={comment.id} />
    </div>
  )
};

SingleComment.propTypes = {
  post: PropTypes.object,
  comment: PropTypes.object
}
export default SingleComment;