import React, { useState, useEffect } from 'react';
import '../Reactions.css';
import like from '../../../common/images/icons/like.png'
import dislike from '../../../common/images/icons/dislike.png'
import { getCommentReactions, putCommentReaction } from '../../../requests/commentRequests';
import { useContext } from 'react';
import AuthContext from '../../../providers/AuthContext';
import Error from '../../Base/Error/Error';
import PropTypes from 'prop-types';

const CommentReaction = ({ postId, commentId }) => {

  const [error, setError] = useState(null);
  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [liked, setLiked] = useState(null);
  const [disliked, setDisliked] = useState(null);

  const { user } = useContext(AuthContext);

  const setVotes = (result) => {
    const filteredLikes = result.data.filter(reaction => reaction.type === 'like').length;
    const filteredDislikes = result.data.filter(reaction => reaction.type === 'dislike').length;
    setLiked(result.data.some(reaction => (reaction.users_id === user.id && reaction.type === "like")));
    setDisliked(result.data.some(reaction => (reaction.users_id === user.id && reaction.type === "dislike")));
    setLikes(filteredLikes);
    setDislikes(filteredDislikes);
  }

  useEffect(() => {
    getCommentReactions(postId, commentId)
      .then(result => {
        if (result.error) {
          throw new Error(result.error);
        }
        setVotes(result);
      }).catch(err => setError(err.message));
  }, [postId, commentId]);

  const upvote = async () => {
    putCommentReaction(postId, commentId, 'like').then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      setVotes(result);
    }).catch(err => setError(err.message));
  }

  const downvote = async () => {
    putCommentReaction(postId, commentId, 'dislike').then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      setVotes(result);
    }).catch(err => setError(err.message));
  }

  if (user.isBanned) {
    return null;
  }

  return (
    <div>
      <div className="comment-reactions-container">
        <div className={liked ? "liked" : "like-container"}>
          <p className="reactions-text">{likes}</p>
          <img src={like} width="30px" height="30px" alt="like-icon" onClick={upvote} />
        </div>
        <div className={disliked ? "disliked" : "dislike-container"}>
          <img src={dislike} width="30px" height="30px" alt="dislike-icon" onClick={downvote} />
          <p className="reactions-text">{dislikes}</p>
        </div>
      </div>
    </div>
  )

}

CommentReaction.propTypes = {
  postId: PropTypes.number,
  commentId: PropTypes.number
}

export default CommentReaction;