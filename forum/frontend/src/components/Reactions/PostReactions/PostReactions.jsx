import React, { useState, useEffect } from 'react';
import '../Reactions.css';
import like from '../../../common/images/icons/like.png'
import dislike from '../../../common/images/icons/dislike.png'
import { getAllPostReactions, putPostReaction } from '../../../requests/postRequests';
import { useContext } from 'react';
import AuthContext from '../../../providers/AuthContext';
import PropTypes from 'prop-types';

const PostReaction = ({ id }) => {

  const [error, setError] = useState(null);
  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [liked, setLiked] = useState(null);
  const [disliked, setDisliked] = useState(null);

  const { user } = useContext(AuthContext);

  const setVotes = (result) => {

    const filteredLikes = result.data.filter(reaction => reaction.type === 'like').length;
    const filteredDislikes = result.data.filter(reaction => reaction.type === 'dislike').length;
    setLiked(result.data.some(reaction => (reaction.users_id === user.id && reaction.type === "like")));
    setDisliked(result.data.some(reaction => (reaction.users_id === user.id && reaction.type === "dislike")));
    setLikes(filteredLikes);
    setDislikes(filteredDislikes);
  }

  useEffect(() => {
    getAllPostReactions(id)
      .then(result => {
        if (result.error) {
          throw new Error(result.error);
        }
        setVotes(result);
      }).catch(err => setError(err.message));
  }, [id]);

  const upvote = async () => {
    putPostReaction(id, 'like').then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      setVotes(result);
    }).catch(err => setError(err.message));
  }

  const downvote = async () => {
    putPostReaction(id, 'dislike').then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      setVotes(result);
    }).catch(err => setError(err.message));
  }


  return (
    <div>
      <div className="reactions-container">
        <div className={liked ? "liked" : "like-container"}>
          <p className="reactions-text">{likes}</p>
          <img src={like} width="30px" height="30px" alt="like-icon" onClick={upvote} />
        </div>
        <div className={disliked ? "disliked" : "dislike-container"}>
          <img src={dislike} width="30px" height="30px" alt="dislike-icon" onClick={downvote} />
          <p className="reactions-text">{dislikes}</p>
        </div>
      </div>
    </div>
  )

}

PostReaction.propTypes = {
  id: PropTypes.number
}
export default PostReaction;