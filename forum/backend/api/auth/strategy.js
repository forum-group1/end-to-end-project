import passportJwt from 'passport-jwt';
import { PRIVATE_KEY } from '../common/auth-config.js';

const options = {
  secretOrKey: PRIVATE_KEY,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const userData = {
    id: payload.id,
    name: payload.name,
    username: payload.username,
    role: payload.role,
    isBanned: payload.isBanned,
  };
  // userData will be set as `req.user` in the `next` middleware
  done(null, userData); // req.user = user
});

export default jwtStrategy;
