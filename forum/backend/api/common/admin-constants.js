export const DEFAULT_BAN_DURATION = 7 * 24 * 60 * 60 * 1000; // one week in miliseconds

export const DEFAULT_BAN_DURATION_DAYS = 7; // one week in miliseconds