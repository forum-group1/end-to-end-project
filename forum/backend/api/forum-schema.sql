-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `forum` DEFAULT CHARACTER SET utf8mb3 ;
USE `forum` ;

-- -----------------------------------------------------
-- Table `forum`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `role` INT(3) NOT NULL DEFAULT 1,
  `banned_until` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum`.`posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`posts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `content` LONGTEXT NULL DEFAULT NULL,
  `created_on` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
  `is_deleted` TINYINT(1) NULL DEFAULT 0,
  `is_locked` TINYINT(1) NULL DEFAULT 0,
  `is_flagged` TINYINT(1) NULL DEFAULT 0,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_Posts_Users_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_Posts_Users`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`comments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `content` LONGTEXT NOT NULL,
  `created_on` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
  `posts_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `is_deleted` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_Comments_Posts1_idx` (`posts_id` ASC) VISIBLE,
  INDEX `fk_comments_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_Comments_Posts1`
    FOREIGN KEY (`posts_id`)
    REFERENCES `forum`.`posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 49
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum`.`comment_reactions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`comment_reactions` (
  `type` VARCHAR(50) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `comments_id` INT(11) NOT NULL,
  PRIMARY KEY (`users_id`, `comments_id`),
  INDEX `fk_comment_reactions_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_comment_reactions_comments1_idx` (`comments_id` ASC) VISIBLE,
  CONSTRAINT `fk_comment_reactions_comments1`
    FOREIGN KEY (`comments_id`)
    REFERENCES `forum`.`comments` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_comment_reactions_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum`.`followers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`followers` (
  `follower_id` INT(11) NOT NULL,
  `followee_id` INT(11) NOT NULL,
  PRIMARY KEY (`follower_id`, `followee_id`),
  INDEX `fk_followers_users2_idx` (`followee_id` ASC) VISIBLE,
  CONSTRAINT `fk_followers_users1`
    FOREIGN KEY (`follower_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_followers_users2`
    FOREIGN KEY (`followee_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum`.`notifications`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`notifications` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `flagger_id` INT(11) NULL DEFAULT NULL,
  `poster_id` INT(11) NOT NULL,
  `posts_id` INT(11) NOT NULL,
  `reason` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_Notifications_Users1_idx` (`flagger_id` ASC) VISIBLE,
  INDEX `fk_Notifications_Users2_idx` (`poster_id` ASC) VISIBLE,
  INDEX `fk_Notifications_Posts1_idx` (`posts_id` ASC) VISIBLE,
  CONSTRAINT `fk_Notifications_Posts1`
    FOREIGN KEY (`posts_id`)
    REFERENCES `forum`.`posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Notifications_Users1`
    FOREIGN KEY (`flagger_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Notifications_Users2`
    FOREIGN KEY (`poster_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum`.`post_reactions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`post_reactions` (
  `type` VARCHAR(45) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `posts_id` INT(11) NOT NULL,
  PRIMARY KEY (`users_id`, `posts_id`),
  INDEX `fk_reactions_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_reactions_posts1_idx` (`posts_id` ASC) VISIBLE,
  CONSTRAINT `fk_reactions_posts1`
    FOREIGN KEY (`posts_id`)
    REFERENCES `forum`.`posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_reactions_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `forum`.`tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`tokens` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `token` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `token_UNIQUE` USING HASH (`token`) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 113
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
