import bcrypt from 'bcrypt';
import userRoles from '../common/user-role.js';

// create a default password
const seededUserPssword = '1234567';
// hash the password in 10 rounds of calculations
const hashPassword = async () => {
  return await bcrypt.hash(seededUserPssword, 10);
};
const hashedUserPssword = await hashPassword();

const users = [
  {
    name: 'Alex',
    username: 'alex1996',
    password: hashedUserPssword,
    role: userRoles.ADMIN,
  },
  {
    name: 'Emma',
    username: 'emma_smith',
    password: hashedUserPssword,
    role: userRoles.USER,
  },
  {
    name: 'Jane',
    username: 'John',
    password: hashedUserPssword,
    role: userRoles.USER,
  },
  {
    name: 'Ben',
    username: 'benjamin',
    password: hashedUserPssword,
    role: userRoles.USER,
  },
];

const posts = [
  {
    title: 'The Very First Post!',
    content: 'Hello world...1',
    users_id: 1,
  },
  {
    title: 'The Second Post!',
    content: 'Hello world...2',
    users_id: 2,
  },
  {
    title: 'The Third Post!',
    content: 'Hello world...3',
    users_id: 3,
  },
  {
    title: 'The Fourth Post!',
    content: 'Hello world...4',
    users_id: 4,
  },
  {
    title: 'The Fifth Post!',
    content: 'Hello world...5',
    users_id: 4,
  },
  {
    title: 'The Sixth Post!',
    content: 'Hello world...6',
    users_id: 1,
  },
];

const comments = [
  {
    content: 'Comment #1',
    posts_id: 1,
    users_id: 1,
  },
  {
    content: 'Comment #2',
    posts_id: 2,
    users_id: 2,
  },
  {
    content: 'Comment #3',
    posts_id: 3,
    users_id: 3,
  },
  {
    content: 'Comment #4',
    posts_id: 4,
    users_id: 4,
  },
  {
    content: 'Comment #5',
    posts_id: 5,
    users_id: 1,
  },
  {
    content: 'Comment #6',
    posts_id: 6,
    users_id: 3,
  },
  {
    content: 'Comment #7',
    posts_id: 6,
    users_id: 3,
  },
  {
    content: 'Comment #1 Again',
    posts_id: 2,
    users_id: 2,
  },
  {
    content: 'Comment #4 Again',
    posts_id: 4,
    users_id: 4,
  },
];


export {
  users,
  posts,
  comments,
};
