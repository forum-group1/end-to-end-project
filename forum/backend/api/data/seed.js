
import pool from './pool.js';
import { users, posts, comments } from './seed-data.js';
(async () => {
  // insert default users
  for (const user of users) {
    await pool.query(`
      INSERT INTO users (name, username, password, role)
      VALUES (?, ?, ?, ?)
    `, [user.name, user.username, user.password, user.role]);
  }

  // insert default posts
  for (const post of posts) {
    await pool.query(`
    INSERT INTO posts (title, content, users_id)
    VALUES (?, ?, ?)
    `, [post.title, post.content, post.users_id]);
  }

  // insert default comments
  for (const comment of comments) {
    await pool.query(`
    INSERT INTO comments (content, posts_id, users_id)
    VALUES (?, ?, ?)
    `, [comment.content, comment.posts_id, comment.users_id]);
  }
  pool.end();
})();
