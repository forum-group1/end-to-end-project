/* eslint-disable max-len */
import pool from './pool.js';

/**
 * Sends SQL request to database to get comment by commentId
 * @param {number} commentId id of the comment
 * @return {object} comment data or null if not found
 */
export const getCommentById = async (commentId) => {
  const sqlSelectCommentById = `
    SELECT c.id as id, posts_id, users_id, name, c.is_deleted as is_deleted, content
    FROM comments as c
    JOIN users as u on c.users_id = u.id 
    WHERE c.id = ? AND c.is_deleted = 0
  `;
  // run the query to get the Comment that was just inserted
  const createdComment = (await pool.query(sqlSelectCommentById, [commentId]));

  if (createdComment) {
    return createdComment[0];
  }
  return null;
};

/**
 * Sends SQL request to database to get comments by postId
 * @param {number} postId id of the comment
 * @return {object} comments data or null if not found
 */
export const getAllCommentsByPost = async (postId) => {
  const sqlGetAllCommentsByPost = `
  SELECT c.id as id, posts_id, users_id, name, c.is_deleted as is_deleted, content
  FROM comments as c
  JOIN users as u on c.users_id = u.id
  WHERE c.posts_id = ? AND c.is_deleted = 0;
  `;
  const result = await pool.query(sqlGetAllCommentsByPost, [postId]);
  return result;
};

/**
 * Sends SQL request to database to get comment by commentId
 * @param {string} comment content of the comment
 * @param {number} userId id of the user making the comment
 * @param {number} postId id of the post the comment belongs to
 * @return {object} comment data or null if not found
 */
export const createComment = async (comment, userId, postId) => {
  const sqlInsertComment = `
    INSERT INTO Comments (content, posts_id, users_id)
    VALUES (?, ?, ?)
  `;
  // run the query that inserts the Comment
  const result = await pool.query(sqlInsertComment, [comment.content, postId, userId]);
  return getCommentById(result.insertId);
};

/**
 * Sends SQL request to database to get comment by commentId
 * @param {number} commentId id of the comment to update
 * @param {string} comment content of the comment
 * @return {object} comment data or null if not found
 */
export const updateComment = async (commentId, comment) => {
  const sqlUpdateComment = `
    UPDATE comments
    SET comments.content = ?
    WHERE comments.id = ?;
  `;
  await pool.query(sqlUpdateComment, [comment.content, commentId]);
  return getCommentById(commentId);
};

/**
 * Sends SQL request to database to delete a comment
 * @param {number} postId id of the post
 * @param {number} commentId id of the comment to update
 * @return {object} comments data or null if not found
 */
export const deleteComment = async (postId, commentId) => {
  const sqlUpdateComment = `
    UPDATE Comments
    SET comments.is_deleted = 1
    WHERE (Comments.id = ?);
  `;
  await pool.query(sqlUpdateComment, [commentId]);
  return await getAllCommentsByPost(postId);
};
