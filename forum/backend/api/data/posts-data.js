/* eslint-disable max-len */
import pool from './pool.js';
export const postExists = async (postId) => {
  return Boolean(await getPostById(postId));
};

/**
 * Sends SQL request to database to get post by id
 * @param {number} postId id of the post
 * @return {object} post data or null if not found
 */
export const getPostById = async (postId) => {
  const sqlGetPostById = `
  SELECT posts.id, posts.title, posts.content, posts.created_on, posts.is_deleted, posts.is_locked, posts.is_flagged, posts.users_id, users.username, users.name
  FROM posts
  JOIN users ON users.id = posts.users_id
  WHERE posts.id = ? AND posts.is_deleted = 0
  `;

  const result = await pool.query(sqlGetPostById, [postId]);
  if (result[0]) {
    return result[0];
  }
  return null;
};

/**
 * Sends SQL request to database to get post by username
 * @param {string} username username of post creator
 * @return {object} post data or null if not found
 */
export const getPostsByUsername = async (username) => {
  const sqlGetPostsByUsername = `
    SELECT posts.id, posts.title, posts.content, posts.created_on, posts.is_deleted, posts.is_locked, posts.is_flagged, posts.users_id, users.username, users.name
    FROM posts
    JOIN users ON users.id = posts.users_id
    WHERE users.username = ? 
    AND posts.is_deleted = 0;
  `;
  const result = await pool.query(sqlGetPostsByUsername, [username]);

  if (result[0]) {
    return result;
  }
  return null;
};

/**
 * Sends SQL request to database to get all posts
 * @return {object} posts data or null if not found
 */
export const getAllPosts = async () => {
  const sqlGetAllPosts = `
  SELECT posts.id, posts.title, posts.content, posts.created_on, posts.is_deleted, posts.is_locked, posts.is_flagged, posts.users_id, users.username, users.name
  FROM posts
  JOIN users ON users.id = posts.users_id
  WHERE posts.is_deleted = 0;
  `;

  const result = await pool.query(sqlGetAllPosts);
  if (result[0]) {
    return result;
  }
  return null;
};

/**
 * Sends SQL request to database to get posts of followed users
 * @param {string} userId id of person logged in
 * @return {object} post data or null if not found
 */
export const getFollowersPosts = async (userId) => {
  const sqlGetFollowersPosts = `
  SELECT posts.id, posts.title, posts.content, posts.created_on, posts.is_deleted, posts.is_locked, posts.is_flagged, posts.users_id, users.username, users.name
  FROM posts
  JOIN users ON users.id = posts.users_id
  WHERE posts.is_deleted = 0 
  AND users.is_deleted=0
  AND posts.users_id IN 
    ( SELECT followee_id
      FROM followers
      WHERE follower_id = ?
    )
  ORDER BY posts.created_on;
  `;

  const result = await pool.query(sqlGetFollowersPosts, [userId]);

  if (!result[0]) {
    return null;
  }
  return result;
};

/**
 * Sends SQL request to database to create a post
 * @param {object} post post data
 * @return {object} post data or null if error
 */
export const createPost = async (post) => {
  const sqlInsertPost = `
    INSERT INTO posts (title, content, users_id)
    VALUES (?, ?, ?)
  `;
  // run the query that inserts the post
  const result = await pool.query(sqlInsertPost, [post.title, post.content, post.users_id]);

  const sqlSelectPostById = `
    SELECT *
    FROM posts
    WHERE posts.id = ?
  `;
  // run the query to get the post that was just inserted
  const createdPost = (await pool.query(sqlSelectPostById, [result.insertId]))[0];
  return createdPost;
};

/**
 * Sends SQL request to database to update a post
 * @param {number} postId post id to update
 * @param {object} body new post data
 * @return {object} post data or null if error
 */
export const updatePost = async (postId, body) => {
  const sqlUpdatePost = `
    UPDATE posts
    SET posts.title = ?, posts.content = ?
    WHERE (posts.id = ?);
  `;

  const result = await pool.query(sqlUpdatePost, [body.title, body.content, postId]);

  if (result.affectedRows === 1) {
    const sqlSelectPostById = `
    SELECT *
    FROM posts
    WHERE posts.id = ?
  `;
    // run the query to get the post that was just updated
    const updatedPost = (await pool.query(sqlSelectPostById, [postId]))[0];
    return updatedPost;
  }
  return null;
};

/**
 * Sends SQL request to database to delete a post
 * @param {number} postId id of post to delete
 * @return {object} post data or null if error
 */
export const deletePost = async (postId) => {
  const sqlUpdatePost = `
    UPDATE posts
    SET posts.is_deleted = 1
    WHERE (posts.id = ?);
  `;

  const result = await pool.query(sqlUpdatePost, [postId]);

  if (result.affectedRows === 1) {
    // run the query to get the post that was just deleted
    const remainingPosts = getAllPosts();
    return remainingPosts;
  }
  return null;
};

/**
 * Sends SQL request to database to flag a post
 * @param {number} postId id of post to flag
 * @param {number} userId id of uesr who made the post
 * @return {object} post data or null if error
 */
export const flagPost = async (postId, userId) => {
  const sqlFlagPost = `
  UPDATE posts
  SET is_flagged = 1
  WHERE id = ?
  `;

  await pool.query(sqlFlagPost, [postId]);
  return;
};

export const lockPost = async (postId) => {
  const sqlLockPost = `
  UPDATE posts
  SET is_locked = 1
  WHERE id = ?
  `;

  await pool.query(sqlLockPost, [postId]);

  const lockedPost = await getPostById(postId);
  return lockedPost;
};
