import pool from './pool.js';

export const userExists = async (username) => {
  return Boolean((await getUserByUsername(username)));
};

/**
 * Sends SQL request to database to get user by id
 * @param {number} userId user id of user to get
 * @return {string} user or null if not found
 */
export const getUserById = async (userId) => {
  const sql = `
  SELECT * 
  FROM users
  WHERE users.id = ?
  AND users.is_deleted = false
  `;

  const result = await pool.query(sql, [userId]);
  // return the first user in the array
  if (result[0]) {
    return result[0];
  }
  return null;
};

/**
 * Sends SQL request to database to get user by username
 * @param {string} username username of user to get
 * @return {string} user or null if not found
 */
export const getUserByUsername = async (username) => {
  const sqlGetUserByUsername = `
    SELECT * 
    FROM users 
    WHERE users.username = ?
    AND users.is_deleted = false
    `;

  const result = await pool.query(sqlGetUserByUsername, [username]);

  // return the first user in the array
  if (result[0]) {
    return result[0];
  }
  // if user does not exist, return null
  return null;
};

/**
 * Sends SQL request to database to get all users
 * @return {string} users or null if not found
 */
export const getAllUsers = async () => {
  const sqlGetAllUsers = `
  SELECT *
  FROM users
  WHERE users.is_deleted = false
  `;

  const result = await pool.query(sqlGetAllUsers);

  if (result[0]) {
    return result;
  }
  return null;
};

/**
 * Sends SQL request to database to create a new user
 * @param {object} user user information
 * @param {string} hashedPassword hashed password of new user
 * @param {number} role role of new user
 * @return {string} user or null if error
 */
export const createUser = async (user, hashedPassword, role) => {
  const sqlInsertUser = `
    INSERT INTO users (name, username, password, role)
    VALUES (?, ?, ?, ?)
  `;
  // run the query that inserts the user
  // eslint-disable-next-line max-len
  const result = await pool.query(sqlInsertUser, [user.name, user.username, hashedPassword, role]);

  const sqlSelectUserById = `
    SELECT *
    FROM users
    WHERE users.id = ?
  `;
  // run the query to get the user that was just inserted
  // eslint-disable-next-line max-len
  const createdUser = (await pool.query(sqlSelectUserById, [result.insertId]))[0];

  return createdUser;
};

/**
 * Sends SQL request to database to delete user by username
 * @param {string} username username of user to delete
 * @return {string} user or null if error
 */
export const deleteUser = async (username) => {
  const sqlDeleteUser = `
    UPDATE users
    SET users.is_deleted = ?
    WHERE users.username = ?
  `;
  // run the query to update the is_deleted field
  const result = await pool.query(sqlDeleteUser, [true, username]);

  if (result.affectedRows === 1) {
    const sqlGetUserByUsername = `
      SELECT * 
      FROM users 
      WHERE users.username = ?
    `;
    // run the query to get the user that was just deleted
    const deletedUser = await pool.query(sqlGetUserByUsername, [username]);

    return deletedUser;
  }
  return null;
};

/**
 * Sends SQL request to database to ban user by username
 * @param {string} username username of user to ban
 * @param {datetime} bannedUntilDate date to ban until
 * @return {string} user or null if error
 */
export const banUser = async (username, bannedUntilDate) => {
  const sqlBanUser = `
    UPDATE users
    SET users.banned_until = ?
    WHERE users.username = ?
  `;
  // run the query to update the banned_until field
  const result = await pool.query(sqlBanUser, [bannedUntilDate, username]);

  if (result.affectedRows === 1) {
    const sqlGetUserByUsername = `
      SELECT * 
      FROM users 
      WHERE users.username = ?
    `;
    // run the query to get the user that was just banned
    const bannedUser = await pool.query(sqlGetUserByUsername, [username]);

    return bannedUser;
  }
  return null;
};

/**
 * Sends SQL request to database to see if users are friends
 * @param {number} userId user id of yourself
 * @param {number} friendId user id of other user
 * @return {boolean} friendship status
 */
export const findFriendship = async (userId, friendId) => {
  const sqlFindFriendship = `
    SELECT * 
    FROM followers
    JOIN users ON followers.followee_id = users.id
    WHERE follower_id = ? AND followee_id = ?
    AND users.is_deleted = false
  `;

  const friendship = await pool.query(sqlFindFriendship, [userId, friendId]);
  if (friendship[0]) {
    return true;
  }
  return false;
};

/**
 * Sends SQL request to database to gett all followees
 * @param {number} userId user id of yourself
 * @return {object} friends data
 */
export const getAllFriends = async (userId) => {
  const sqlGetAllFriends = `
    SELECT * 
    FROM followers
    JOIN users ON followers.followee_id = users.id
    WHERE followers.follower_id = ?
    AND users.is_deleted = false
  `;

  const allFriends = await pool.query(sqlGetAllFriends, [userId]);
  if (!allFriends[0]) {
    return null;
  }
  return allFriends;
};

/**
 * Sends SQL request to database to add a friend
 * @param {number} userId user id of yourself
 * @param {number} friendId user id of other user
 * @return {boolean} friendship status
 */
export const addFriend = async (userId, friendId) => {
  const sqlAddFriend = `
    INSERT
    INTO followers(follower_id, followee_id)
    VALUES(?,?);
  `;
  try {
    await pool.query(sqlAddFriend, [userId, friendId]);
    const friendship = findFriendship(userId, friendId);
    return friendship;
  } catch (error) {
    return null;
  }
};

/**
 * Sends SQL request to database to delete a friend
 * @param {number} userId user id of yourself
 * @param {number} friendId user id of other user
 * @return {boolean} friendship status
 */
export const deleteFriend = async (userId, friendId) => {
  const friendship = await findFriendship(userId, friendId);
  if (!friendship) {
    return null;
  }

  const sqlDeleteFriend = `
    DELETE
    FROM followers
    WHERE follower_id = ? AND followee_id = ?;
  `;
  await pool.query(sqlDeleteFriend, [userId, friendId]);

  return friendship;
};


