/* eslint-disable max-len */
import pool from './pool.js';

/**
 * Sends SQL request to database to get reactions of a post
 * @param {number} postId id of the post the reactions belongs to
 * @return {object} reactions data or null if not found
 */
export const getPostReactionsByPostId = async (postId) => {
  const sqlSelectPostReactionByIds = `
    SELECT *
    FROM post_reactions
    WHERE posts_id = ?
  `;
  // run the query to get the reaction by userId and postId
  const reactions = (await pool.query(sqlSelectPostReactionByIds, [postId]));
  if (reactions[0]) {
    return reactions;
  }
  return null;
};

/**
 * Sends SQL request to database to get reactions of a post by ids
 * @param {number} userId id of the user the reactions belongs to
 * @param {number} postId id of the post the reactions belongs to
 * @return {object} reactions data or null if not found
 */
export const getPostReactionByIds = async (userId, postId) => {
  const sqlSelectPostReactionByIds = `
    SELECT *
    FROM post_reactions
    WHERE users_id = ? AND posts_id = ?
  `;
  // run the query to get the reaction by userId and postId
  const reaction = (await pool.query(sqlSelectPostReactionByIds, [userId, postId]));
  if (reaction[0]) {
    return reaction[0];
  }
  return null;
};

/**
 * Sends SQL request to database to create a new post reaction
 * @param {number} userId id of the user the reactions belongs to
 * @param {number} postId id of the post the reactions belongs to
 * @param {number} type type of reaction
 * @return {object} reactions data or null if not found
 */
export const insertPostReaction = async (userId, postId, type) => {
  const sqlInsertPostReaction = `
  INSERT into post_reactions(type, users_id, posts_id) 
  VALUES (?, ?, ?);
  `;
  await pool.query(sqlInsertPostReaction, [type, userId, postId]);
  const postReaction = getPostReactionByIds(userId, postId);
  if (postReaction) {
    return postReaction;
  }
  return null;
};

/**
 * Sends SQL request to database to update a post reaction
 * @param {number} userId id of the user the reactions belongs to
 * @param {number} postId id of the post the reactions belongs to
 * @param {number} type type of reaction
 * @return {object} reactions data or null if not found
 */
export const updatePostReaction = async (userId, postId, type) => {
  const sqlUpdatePostReaction = `
  UPDATE post_reactions 
  SET type = ?
  WHERE users_id = ? AND posts_id =?;
  `;
  await pool.query(sqlUpdatePostReaction, [type, userId, postId]);
  const postReaction = getPostReactionByIds(userId, postId);
  if (postReaction) {
    return postReaction;
  }
  return null;
};

/**
 * Sends SQL request to database to delete post reaction
 * @param {number} userId id of the user the reactions belongs to
 * @param {number} postId id of the post the reactions belongs to
 * @return {object} reactions data or null if not found
 */
export const deletePostReaction = async (userId, postId) => {
  const sqlDeletePostReaction = `
  DELETE  
  FROM post_reactions
  WHERE users_id = ? AND posts_id = ?;
  `;

  await pool.query(sqlDeletePostReaction, [userId, postId]);

  const reactions = await getPostReactionsByPostId(postId);
  if (reactions) {
    return reactions;
  }
  return [];
};

/**
 * Sends SQL request to database to get reactions of a comment
 * @param {number} commentId id of the comment the reactions belongs to
 * @return {object} reactions data or null if not found
 */
export const getCommentReactionsByCommentId = async (commentId) => {
  const sqlSelectPostReactionByIds = `
    SELECT *
    FROM comment_reactions
    WHERE comments_id = ?
  `;
  // run the query to get the reaction by userId and commentId
  const reactions = (await pool.query(sqlSelectPostReactionByIds, [commentId]));
  if (reactions[0]) {
    return reactions;
  }
  return null;
};

/**
 * Sends SQL request to database to get reactions of a comment by ids
 * @param {number} userId id of the user the reactions belongs to
 * @param {number} commentId id of the comment the reactions belongs to
 * @return {object} reactions data or null if not found
 */
export const getCommentReactionByIds = async (userId, commentId) => {
  const sqlSelectCommentReactionByIds = `
    SELECT *
    FROM comment_reactions
    WHERE users_id = ? AND comments_id = ?
  `;
  // run the query to get the reaction by userId and commentId
  const reaction = await pool.query(sqlSelectCommentReactionByIds, [userId, commentId]);
  if (reaction[0]) {
    return reaction[0];
  }
  return null;
};

/**
 * Sends SQL request to database to create a new comment reaction
 * @param {number} userId id of the user the reactions belongs to
 * @param {number} commentId id of the comment the reactions belongs to
 * @param {number} type type of reaction
 * @return {object} reactions data or null if not found
 */
export const insertCommentReaction = async (userId, commentId, type) => {
  const sqlInsertCommentReaction = `
  INSERT into comment_reactions(type, users_id, comments_id) 
  VALUES (?, ?, ?);
  `;
  await pool.query(sqlInsertCommentReaction, [type, userId, commentId]);
  const commentReaction = await getCommentReactionByIds(userId, commentId);
  if (commentReaction) {
    return commentReaction;
  }
  return null;
};

/**
 * Sends SQL request to database to update a comment reaction
 * @param {number} userId id of the user the reactions belongs to
 * @param {number} commentId id of the comment the reactions belongs to
 * @param {number} type type of reaction
 * @return {object} reactions data or null if not found
 */
export const updateCommentReaction = async (userId, commentId, type) => {
  const sqlUpdateCommentReaction = `
  UPDATE comment_reactions 
    SET type = ?
    WHERE users_id = ? AND comments_id =?;
  `;
  await pool.query(sqlUpdateCommentReaction, [type, userId, commentId]);
  const commentReaction = await getCommentReactionByIds(userId, commentId);
  if (commentReaction) {
    return commentReaction;
  }
  return null;
};

/**
 * Sends SQL request to database to delete a comment reaction
 * @param {number} userId id of the user the reactions belongs to
 * @param {number} commentId id of the comment the reactions belongs to
 * @return {object} reactions data or null if not found
 */
export const deleteCommentReaction = async (userId, commentId) => {
  const sqlDeletePostReaction = `
  DELETE  
  FROM comment_reactions
  WHERE users_id = ? AND comments_id = ?;
  `;

  await pool.query(sqlDeletePostReaction, [userId, commentId]);

  const reactions = await getCommentReactionsByCommentId(commentId);
  if (reactions) {
    return reactions;
  }
  return [];
};
