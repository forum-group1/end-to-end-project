import pool from './pool.js';

/**
 * Sends SQL request to database to whitelist a token
 * @param {string} token token to whitelist
 * @return {string} whitelisted token
 */
export const whitelistToken = async (token) => {
  const sqlInsertToken = `
    INSERT INTO tokens (token)
    VALUES (?)
  `;
  const result = await pool.query(sqlInsertToken, [token]);

  const sqlGetToken = `
  SELECT *
  FROM tokens
  WHERE tokens.id = ?
`;

  // run the query to get the token that was just inserted
  const whitelistedToken = (await pool.query(sqlGetToken, [result.insertId]))[0];
  return whitelistedToken;
};

/**
 * Sends SQL request to database to delete a token from whitelist
 * @param {string} token token to delete
 * @return {string} deleted token
 */
export const deleteToken = async (token) => {
  const sqlDeleteToken = `
    DELETE FROM tokens
    WHERE tokens.token = ?
  `;
  await pool.query(sqlDeleteToken, [token]);
  return token;
};

/**
 * Sends SQL request to database to see if token is whitelisted
 * @param {string} token token to check
 * @return {boolean} whitelisted token status
 */
export const tokenExists = async (token) => {
  const sqlGetToken = `
    SELECT *
    FROM tokens
    WHERE tokens.token = ?
  `;
  const result = await pool.query(sqlGetToken, [token]);
  return result.length > 0;
};
