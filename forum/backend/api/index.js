import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import passport from 'passport';
import usersRoute from './controllers/users-controller.js';
import postsRoute from './controllers/posts-controller.js';
import authRoute from './controllers/auth-controller.js';
import commentsRoute from './controllers/comments-controller.js';
import adminRoute from './controllers/admin-controller.js';
import reactionsRoute from './controllers/reactions-controller.js';
import jwtStrategy from './auth/strategy.js';
import dotenv from 'dotenv';

const config = dotenv.config().parsed;
const app = express();

const PORT = +config.SERVER_PORT;

passport.use(jwtStrategy);

app.use(passport.initialize());
app.use(helmet());
app.use(cors());
app.use(express.json());

app.use('/users', usersRoute);
app.use('/auth', authRoute);
app.use('/posts', postsRoute);
app.use('/posts', commentsRoute);
app.use('/admin', adminRoute);
app.use('/posts', reactionsRoute);

app.use((err, req, res, next) => {
  // logger.log(err)
  res.status(500).send({
    // eslint-disable-next-line max-len
    error: 'An unexpected error occurred, our developers are working hard to resolve it.',
  });
});

app.all('*', (req, res) =>
  res.status(404).send({ error: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
