/* eslint-disable consistent-return */
/* eslint-disable max-len */
import express from 'express';
import errors from '../services/errors.js';
import validateBody from '../middlewares/validate-body.js';
import createCommentValidator from '../validators/create-comment-validator.js';
import * as commentsData from '../data/comments-data.js';
import {
  getAllCommentsByPost,
  createComment,
  updateComment,
  deleteComment,
} from '../services/comments-service.js';
import { authMiddleware, roleMiddleware } from '../middlewares/authenticate-user.js';
import { loggedUserGuard } from '../middlewares/logged-user-guard.js';
import role from '../common/user-role.js';


// eslint-disable-next-line new-cap
const commentsRoute = express.Router();

commentsRoute.use(authMiddleware);
commentsRoute.use(loggedUserGuard);
commentsRoute.use(roleMiddleware(role.ADMIN, role.USER));

/**
 * Calls service function to get a comments by post id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
commentsRoute.get('/:postId/comments', async (req, res) => {
  // if an id is submitted, get comments by postId
  const postId = req.params.postId;
  if (postId) {
    const result = await getAllCommentsByPost(commentsData)(+postId);

    if (result.error === errors.NOT_FOUND) {
      return res.status(404).json({ error: `Post not found!` });
    }
    return res.status(200).json(result);
  }
});

/**
 * Calls service function to create a comment by post id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
commentsRoute.post('/:postId/comments', authMiddleware, validateBody(createCommentValidator), async (req, res) => {
  const userId = req.user.id;
  const userRole = req.user.role;
  const postId = req.params.postId;
  const result = await createComment(commentsData)(req.body, +userId, +postId, userRole);
  // if an error occured, return it
  if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ error: `Invalid request!` });
  } else if (result.error === errors.UNAUTHORIZED) {
    return res.status(401).json({ error: `Unauthorized delete attempt!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ error: `Post is locked and cannot be updated, commented or reacted on` });
  } else if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ error: `Post not found!` });
  }
  // respond with the created post
  res.status(200).json(result);
});

/**
 * Calls service function to update a comment by post id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
commentsRoute.put('/:postId/comments/:commentId', authMiddleware, async (req, res) => {
  const { postId, commentId } = req.params;
  const user = req.user;
  const result = await updateComment(commentsData)(+postId, +commentId, +user.id, user.role, req.body);
  // if an error occured, return it
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ error: `Post or comment not found!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ error: `Invalid request!` });
  } else if (result.error === errors.UNAUTHORIZED) {
    return res.status(401).json({ error: `Unauthorized delete attempt!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ error: `Post is locked and cannot be updated, commented or reacted on` });
  }
  // respond with the updated post
  res.status(200).json(result);
});

/**
 * Calls service function to delete a comment by post id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
commentsRoute.delete('/:postId/comments/:commentId', async (req, res) => {
  const { postId, commentId } = req.params;
  const user = req.user;
  const result = await deleteComment(commentsData)(+postId, +commentId, +user.id);
  // if an error occured, return it
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ error: `Post or comment not found!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ error: `Invalid request!` });
  } else if (result.error === errors.UNAUTHORIZED) {
    return res.status(401).json({ error: `Unauthorized delete attempt!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ error: `Post is locked and cannot be updated, commented or reacted on` });
  }
  // respond with the updated post
  res.status(200).json(result);
});

export default commentsRoute;
