/* eslint-disable new-cap */
/* eslint-disable max-len */
import express from 'express';
import errors from '../services/errors.js';
import userRole from '../common/user-role.js';
import * as usersData from '../data/users-data.js';
import { banUser, deleteUser } from '../services/user-service.js';
import { authMiddleware, roleMiddleware } from '../middlewares/authenticate-user.js';
import { loggedUserGuard } from '../middlewares/logged-user-guard.js';

const adminRoute = express.Router();

adminRoute.use(authMiddleware);
adminRoute.use(loggedUserGuard);
adminRoute.use(roleMiddleware(userRole.ADMIN));

/**
 * Calls service function to ban user
 * @param {req} req request
 * @param {res} res response
 * @return {res} banned user data or error message
 */
adminRoute.put('/ban/:username', async (req, res) => {
  const result = await banUser(usersData)(req.params.username);
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `User not found!` });
  }
  return res.status(200).json(result);
});

/**
 * Calls service function to delete user
 * @param {req} req request
 * @param {res} res response
 * @return {res} banned user data or error message
 */
adminRoute.put('/delete/:username', async (req, res) => {
  const result = await deleteUser(usersData)(req.params.username);
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `User not found!` });
  }
  return res.status(200).json(result);
});

export default adminRoute;
