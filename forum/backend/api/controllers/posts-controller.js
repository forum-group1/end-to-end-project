/* eslint-disable consistent-return */
/* eslint-disable max-len */
import express from 'express';
import errors from '../services/errors.js';
import validateBody from '../middlewares/validate-body.js';
import createPostValidator from '../validators/create-post-validator.js';
import * as postsData from '../data/posts-data.js';
import {
  getPostById,
  getAllPosts,
  createPost,
  updatePost,
  deletePost,
  flagPost,
  lockPost,
  getFollowersPosts,
} from '../services/posts-service.js';
import { authMiddleware, roleMiddleware } from '../middlewares/authenticate-user.js';
import { loggedUserGuard } from '../middlewares/logged-user-guard.js';
import role from '../common/user-role.js';

// eslint-disable-next-line new-cap
const postsRoute = express.Router();

/**
 * Calls service function to get all posts
 * @param {req} req request
 * @param {res} res response
 * @return {res} all posts or error message
 */
postsRoute.get('/', async (req, res) => {
  const result = await getAllPosts(postsData)();
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Posts not found!` });
  }
  return res.status(200).json(result);
});

/**
 * Calls service function to get a post by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
postsRoute.get('/:id', async (req, res) => {
  const postId = req.params.id;
  const result = await getPostById(postsData)(postId);

  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Post not found!` });
  }
  return res.status(200).json(result);
});

/**
 * Calls service function to get posts from peope you follow
 * @param {req} req request
 * @param {res} res response
 * @return {res} posts or error message
 */
postsRoute.get('/followers/timeline', authMiddleware, loggedUserGuard, roleMiddleware(role.ADMIN, role.USER), async (req, res) => {
  const userId = req.user.id;
  const result = await getFollowersPosts(postsData)(userId);

  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Posts not found!` });
  }
  return res.status(200).json(result);
});

/**
 * Calls service function to create a post
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
postsRoute.post('/', authMiddleware, loggedUserGuard, roleMiddleware(role.ADMIN, role.USER), validateBody(createPostValidator), async (req, res) => {
  const result = await createPost(postsData)(req.body);
  // if an error occured, return it
  if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ ...result, error: `Invalid request!` });
  }
  // respond with the created post
  res.status(200).json(result);
});

/**
 * Calls service function to update a post by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
postsRoute.put('/:id', authMiddleware, loggedUserGuard, roleMiddleware(role.ADMIN, role.USER), async (req, res) => {
  const userId = req.user.id;
  const userRole = req.user.role;
  const postId = req.params.id;
  const result = await updatePost(postsData)(postId, +userId, userRole, req.body);
  // if an error occured, return it
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Post not found!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ ...result, error: `Invalid request!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ ...result, error: `Post is locked and cannot be updated, commented or reacted on` });
  } else if (result.error === errors.UNAUTHORIZED) {
    return res.status(401).json({ ...result, error: `Unauthorized` });
  }
  // respond with the updated post
  res.status(200).json(result);
});

/**
 * Calls service function to delete a post by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
postsRoute.delete('/:id', authMiddleware, loggedUserGuard, roleMiddleware(role.ADMIN, role.USER), async (req, res) => {
  const postId = req.params.id;
  const userId = req.user.id;
  const username = req.user.username;
  const result = await deletePost(postsData)(postId, +userId, username);
  // if an error occured, return it
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Post not found!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ ...result, error: `Invalid request!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ ...result, error: `Post is locked and cannot be updated, commented or reacted on` });
  } else if (result.error === errors.UNAUTHORIZED) {
    return res.status(401).json({ ...result, error: `Unauthorized` });
  }
  // respond with the updated post
  res.status(200).json(result);
});

/**
 * Calls service function to flag a post by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
postsRoute.put('/:id/flags', authMiddleware, loggedUserGuard, roleMiddleware(role.ADMIN, role.USER), async (req, res) => {
  const postId = +req.params.id;
  const userId = +req.user.id;
  const userRole = req.user.role;
  const username = req.user.username;
  const result = await flagPost(postsData)(postId, userId, userRole, username);
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Post not found!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ ...result, error: `Invalid request!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ ...result, error: `Post is locked and cannot be updated, commented or reacted on` });
  }
  // respond with the updated post
  res.status(200).json(result);
});

/**
 * Calls service function to lock a post by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} post or error message
 */
postsRoute.put('/:id/locks', authMiddleware, loggedUserGuard, roleMiddleware(role.ADMIN, role.USER), async (req, res) => {
  const postId = +req.params.id;
  const userId = req.user.id;
  const userRole = req.user.role;
  const result = await lockPost(postsData)(postId, userId, userRole);

  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Post not found!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ ...result, error: `Invalid request!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ ...result, error: `Post is locked and cannot be updated, commented or reacted on` });
  } else if (result.error === errors.UNAUTHORIZED) {
    return res.status(401).json({ ...result, error: `Unauthorized` });
  }
  // respond with the updated post
  res.status(200).json(result);
});

export default postsRoute;
