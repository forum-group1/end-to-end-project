/* eslint-disable new-cap */
/* eslint-disable max-len */
import express from 'express';
import errors from '../services/errors.js';
import createToken from '../auth/create-token.js';
import * as usersData from '../data/users-data.js';
import * as tokenData from '../data/token-data.js';
import { loginUser } from '../services/user-service.js';
import { whitelistToken, deleteToken } from '../services/token-service.js';

const authRoute = express.Router();

/**
 * Calls service function to login user
 * @param {req} req request
 * @param {res} res response
 * @return {res} token or error message
 */
authRoute.post('/login', async (req, res) => {
  const { error, user } = await loginUser(usersData)(req.body.username, req.body.password);
  // check if the user has an account to login to
  if (error === errors.NOT_FOUND) {
    res.status(404).json({ message: 'Please create an account first.' });
  //   check if the credentials are valid
  } else if (error === errors.INVALID_SIGNIN) {
    res.status(400).json({ message: 'Invalid username/password.' });
  } else {
    const payload = {
      id: user.id,
      name: user.name,
      username: user.username,
      role: user.role,
      isBanned: (user.banned_until !== null && user.banned_until > new Date()) ? true : false,
    };
    // if passed, create a token
    const token = createToken(payload);
    // whitelist the token
    await whitelistToken(tokenData)(token);
    // return sucess status to the user
    res.status(200).json({ token: token });
  }
});

/**
 * Calls service function to logout user
 * @param {req} req request
 * @param {res} res response
 * @return {res} message
 */
authRoute.delete('/logout', async (req, res) => {
  const token = req.headers.authorization.replace('Bearer ', '');
  await deleteToken(tokenData)(token);
  res.status(200).json({ message: 'You have been logged out.' });
});

export default authRoute;
