/* eslint-disable consistent-return */
/* eslint-disable new-cap */
/* eslint-disable max-len */
import express from 'express';
import role from '../common/user-role.js';
import errors from '../services/errors.js';
import validateBody from '../middlewares/validate-body.js';
import createUserValidator from '../validators/create-user-validator.js';
// import freindsValidator from '../validators/friends-validator.js';
import * as usersData from '../data/users-data.js';
import * as postsData from '../data/posts-data.js';
import {
  createUser,
  getAllUsers,
  getUserByUsername,
  addFriend,
  deleteFriend,
  getAllFriends,
  getUserById,
  findFriendship,
} from '../services/user-service.js';
import { getPostsByUsername } from '../services/posts-service.js';
import { authMiddleware, roleMiddleware } from '../middlewares/authenticate-user.js';
import { loggedUserGuard } from '../middlewares/logged-user-guard.js';

const usersRoute = express.Router();

/**
 * Calls service function to get all users
 * @param {req} req request
 * @param {res} res response
 * @return {res} all users or error message
 */
usersRoute.get('/', async (req, res) => {
  const result = await getAllUsers(usersData)();
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Users not found!` });
  }
  return res.status(200).json(result);
});

/**
 * Calls service function to get user by username
 * @param {req} req request
 * @param {res} res response
 * @return {res} user or error message
 */
usersRoute.get('/:username', async (req, res) => {
  const result = await getUserByUsername(usersData)(req.params.username);
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `User not found!` });
  }
  return res.status(200).json(result);
});

/**
 * Calls service function to get user by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} user or error message
 */
usersRoute.get('/id/:userId', async (req, res) => {
  const result = await getUserById(usersData)(req.params.userId);
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `User not found!` });
  }
  return res.status(200).json(result);
});

/**
 * Calls service function to create a new user
 * @param {req} req request
 * @param {res} res response
 * @return {res} user or error message
 */
usersRoute.post('/', validateBody(createUserValidator), async (req, res) => {
  const result = await createUser(usersData)(req.body);
  // if a user already exists, return an error
  if (result.error === errors.DUPLICATE_RECORD) {
    return res.status(400).json({ ...result, error: `User with username ${req.body.username} already exists!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ ...result, error: `Invalid request!` });
  }
  // respond with the created user
  res.status(200).json(result);
});

/**
 * Calls service function to follow a user
 * @param {req} req request
 * @param {res} res response
 * @return {res} user or error message
 */
usersRoute.post('/:friendId/friends', authMiddleware, loggedUserGuard, roleMiddleware(role.ADMIN, role.USER), async (req, res) => {
  const friendId = +req.params.friendId;
  const userId = req.user.id;
  // const friendId = req.body.friendId;
  // const userId = +req.params.id;
  // const userIdAlt = req.user.id;
  // if (userId !== userIdAlt) {
  //   return res.status(400).json({ error: `Invalid request`, data: null });
  // }
  const result = await addFriend(usersData)(userId, friendId);
  if (result.error === errors.DUPLICATE_RECORD) {
    return res.status(400).json({ ...result, error: `This person is already added to friends!` });
  }
  res.status(200).json(result);
});

/**
 * Calls service function to unfollow a user
 * @param {req} req request
 * @param {res} res response
 * @return {res} user or error message
 */
usersRoute.delete('/:friendId/friends', authMiddleware, loggedUserGuard, roleMiddleware(role.ADMIN, role.USER), async (req, res) => {
  const friendId = +req.params.friendId;
  const userId = req.user.id;
  // const friendId = req.body.friendId;
  // const userId = +req.params.id;
  // const userIdAlt = req.user.id;
  // if (userId !== userIdAlt) {
  //   return res.status(400).json({ error: `Invalid request`, data: null });
  // }
  const result = await deleteFriend(usersData)(userId, friendId);
  if (result.error === errors.DUPLICATE_RECORD) {
    return res.status(400).json({ ...result, error: `This person is already added to friends!` });
  }
  if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ ...result, error: `You cannot unfriend a user who is not on your friends list!` });
  }
  res.status(200).json(result);
});

/**
 * Calls service function to get all followed users
 * @param {req} req request
 * @param {res} res response
 * @return {res} users or error message
 */
usersRoute.get('/:id/friends', authMiddleware, async (req, res) => {
  const userId = +req.params.id;
  const result = await getAllFriends(usersData)(userId);

  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Friends not found!` });
  }
  return res.json(result);
});

/**
 * Calls service function to get user by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} user or error message
 */
usersRoute.get('/:friendId/isFriend', authMiddleware, async (req, res) => {
  const friendId = +req.params.friendId;
  const userId = req.user.id;
  const result = await findFriendship(usersData)(userId, friendId);
  return res.json(result);
});

/**
 * Calls service function to get user by username
 * @param {req} req request
 * @param {res} res response
 * @return {res} user or error message
 */
usersRoute.get('/:username/posts', authMiddleware, async (req, res) => {
  const username = req.params.username;
  const result = await getPostsByUsername(postsData)(username);

  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Posts not found!` });
  }
  return res.json(result);
});

export default usersRoute;
