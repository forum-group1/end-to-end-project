/* eslint-disable new-cap */
/* eslint-disable consistent-return */
/* eslint-disable max-len */
import express from 'express';
import errors from '../services/errors.js';
import validateBody from '../middlewares/validate-body.js';
import reactionsValidator from '../validators/reactions-validator.js';
import * as reactionsData from '../data/reactions-data.js';
import { upsertPostReaction, upsertCommentReaction, getPostReactions, getCommentReactions } from '../services/reactions-service.js';
import { authMiddleware, roleMiddleware } from '../middlewares/authenticate-user.js';
import { loggedUserGuard } from '../middlewares/logged-user-guard.js';
import userRole from '../common/user-role.js';

const reactionsRoute = express.Router();

reactionsRoute.use(authMiddleware);
reactionsRoute.use(loggedUserGuard);
reactionsRoute.use(roleMiddleware(userRole.ADMIN, userRole.USER));

/**
 * Calls service function to get reactions of a post
 * @param {req} req request
 * @param {res} res response
 * @return {res} reactions or error message
 */
reactionsRoute.get('/:id/reactions', async (req, res) => {
  const postId = req.params.id;
  const userId = req.user.id;
  const result = await getPostReactions(reactionsData)(+userId, +postId);
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: 'Post or reactions not found!' });
  }
  res.status(200).json(result);
});

/**
 * Calls service function to react to a post
 * @param {req} req request
 * @param {res} res response
 * @return {res} reactions or error message
 */
reactionsRoute.put('/:id/reactions', validateBody(reactionsValidator), async (req, res) => {
  const postId = req.params.id;
  const userId = req.user.id;
  const type = req.body.type;
  const result = await upsertPostReaction(reactionsData)(+userId, +postId, type);
  // if an error occured, return it
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: `Post not found!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ ...result, error: `Invalid request!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ ...result, error: `Post is locked and cannot be updated, commented or reacted on` });
  }
  // respond with reaction info
  res.status(200).json(result);
});

/**
 * Calls service function to get reactions of a comment
 * @param {req} req request
 * @param {res} res response
 * @return {res} reactions or error message
 */
reactionsRoute.get('/:postId/comments/:commentId/reactions', async (req, res) => {
  const { postId, commentId } = req.params;
  const userId = req.user.id;
  const result = await getCommentReactions(reactionsData)(+userId, +postId, +commentId);
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ ...result, error: 'Post, comment or reactions not found!' });
  }
  res.status(200).json(result);
});

/**
 * Calls service function to react to a comment
 * @param {req} req request
 * @param {res} res response
 * @return {res} reactions or error message
 */
reactionsRoute.put('/:postId/comments/:commentId/reactions', validateBody(reactionsValidator), async (req, res) => {
  const { postId, commentId } = req.params;
  const userId = req.user.id;
  const type = req.body.type;
  const result = await upsertCommentReaction(reactionsData)(+userId, +postId, +commentId, type);
  // if an error occured, return it
  if (result.error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `Post not found!` });
  } else if (result.error === errors.INVALID_REQUEST) {
    return res.status(400).json({ message: `Invalid request!` });
  } else if (result.error === errors.POST_LOCKED) {
    return res.status(403).json({ message: `Post is locked and cannot be updated, commented or reacted on` });
  }
  // respond with reaction info
  res.status(200).json(result);
});

export default reactionsRoute;
