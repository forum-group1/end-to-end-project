export default {
  content: (value) => {
    if (!value) {
      return 'Content is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 5000) {
      return 'Content should be a string in range [1..5000]';
    }
    return null;
  },
};
