export default {
  type: (value) => {
    if (!value) {
      return 'Type is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 50) {
      return 'Type should be a string in range [1..50]';
    }
    return null;
  },
};
