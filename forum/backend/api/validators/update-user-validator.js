export default {
  name: (value) => {
    if (!value) {
      return null;
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 20) {
      return 'Name should be a string in range [1..20]';
    }
    return null;
  },
};
