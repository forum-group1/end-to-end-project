export default {
  title: (value) => {
    if (!value) {
      return 'Title is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 70) {
      return 'Title should be a string in range [1..70]';
    }
    return null;
  },
  content: (value) => {
    if (!value) {
      return 'Content is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 255) {
      return 'Content should be a string in range [1..255]';
    }
    return null;
  },
};
