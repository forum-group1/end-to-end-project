export default {
  friendId: (value) => {
    if (!value) {
      return 'FriendId is required';
    }
    if (typeof +value !== 'number' || value < 1) {
      return 'FriendId should be a positive number';
    }
    return null;
  },
};
