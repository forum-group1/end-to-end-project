export default {
  name: (value) => {
    if (!value) {
      return 'Name is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 20) {
      return 'Name should be a string in range [1..20]';
    }
    return null;
  },
  username: (value) => {
    if (!value) {
      return 'Username is required';
    }
    if (typeof value !== 'string' || value.length < 1 || value.length > 20) {
      return 'Username should be a string in range [1..20]';
    }
    return null;
  },
  password: (value) => {
    if (!value) {
      return 'Password is required';
    }
    if (typeof value !== 'string' || value.length < 7 || value.length > 20) {
      return 'Password should be a string in range [7..20]';
    }
    return null;
  },
};
