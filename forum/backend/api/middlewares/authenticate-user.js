import passport from 'passport';

// tells to use the jwt strategy
const authMiddleware = passport.authenticate('jwt', { session: false });

const roleMiddleware = (...roleNames) => {
  return (req, res, next) => {
    let isAuthorized = false;
    for (const roleName of roleNames) {
      if (req.user.role === roleName) {
        isAuthorized = true;
      }
    }
    // if a user is given and their role is appropriate, they pass
    if (req.user && isAuthorized) {
      next();
    } else {
      res.status(403).json({ message: 'Resource is forbidden.' });
    }
  };
};

export {
  authMiddleware,
  roleMiddleware,
};
