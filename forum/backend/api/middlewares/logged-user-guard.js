import { tokenExists } from '../services/token-service.js';
import * as tokenData from '../data/token-data.js';


export const loggedUserGuard = async (req, res, next) => {
  const token = req.headers.authorization.replace('Bearer ', '');

  // if the token does not exist in the table, it has not been whitelisted
  if (!(await tokenExists(tokenData)(token))) {
    return res.status(401).json({ error: 'You are not logged in!' });
  }
  // if the token has been whitelisted, then the user can proceed
  await next();
};
