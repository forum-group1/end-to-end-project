import errors from './errors.js';

/**
 * Calls data functions to whitelist a new token
 *
 * @param {Object} tokenData functions that send queries to the database
 * @return {Object} { error if exists, whitelisted token if no error}
 */
export const whitelistToken = (tokenData) => async (token) => {
  const whitelistedToken = await tokenData.whitelistToken(token);

  if (!whitelistedToken) {
    return {
      error: errors.INVALID_REQUEST,
      data: null,
    };
  }
  return {
    error: null,
    data: whitelistedToken,
  };
};

/**
 * Calls data functions to delete a token from the whitelist
 *
 * @param {Object} tokenData functions that send queries to the database
 * @return {Object} { error if exists, deleted token if no error}
 */
export const deleteToken = (tokenData) => async (token) => {
  const deletedToken = await tokenData.deleteToken(token);

  return {
    error: null,
    data: deletedToken,
  };
};

/**
 * Calls data functions to see if token exists
 *
 * @param {Object} tokenData functions that send queries to the database
 * @return {Boolean} token exists or not
 */
export const tokenExists = (tokenData) => async (token) => {
  return await tokenData.tokenExists(token);
};
