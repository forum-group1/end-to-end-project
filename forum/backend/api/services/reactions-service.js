/* eslint-disable max-len */
import { getCommentById } from '../data/comments-data.js';
import { getPostById } from '../data/posts-data.js';
import errors from './errors.js';
import reactionTypes from '../common/reaction-types.js';

/**
 * Calls data functions to get reactions of a post
 * @param {Object} reactionsData functions that send queries to the database
 * @return {Object} { error if exists, reactions information if no error}
 */
export const getPostReactions = (reactionsData) => async (userId, postId) => {
  const post = await getPostById(postId);
  if (!post) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  const reactions = await reactionsData.getPostReactionsByPostId(postId);
  if (!reactions) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: reactions,
  };
};

/**
 * Calls data functions to create a new reaction of a post
 * @param {Object} reactionsData functions that send queries to the database
 * @return {Object} { error if exists, reactions information if no error }
 */
export const upsertPostReaction = (reactionsData) => async (userId, postId, type) => {
  // if a post with that id does not exist, return an error
  const post = await getPostById(postId);
  if (!post) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  } else if (!(Object.values(reactionTypes).includes(type))) {
    return {
      error: errors.INVALID_REQUEST,
      data: null,
    };
  } else if (post.is_locked === 1) {
    return {
      error: errors.POST_LOCKED,
      data: null,
    };
  }

  const reaction = await reactionsData.getPostReactionByIds(userId, postId, type);
  // create post reaction if it doesn't exist
  if (reaction === null) {
    await reactionsData.insertPostReaction(userId, postId, type);
    const reactions = await reactionsData.getPostReactionsByPostId(postId);
    return {
      error: null,
      data: reactions,
    };
  }
  if (reaction.type === type) {
    const remainingReactions = await reactionsData.deletePostReaction(userId, postId);
    return {
      error: null,
      data: remainingReactions,
    };
  }
  // update post reaction if it already exists;
  await reactionsData.updatePostReaction(userId, postId, type);
  const reactions = await reactionsData.getPostReactionsByPostId(postId);
  return {
    error: null,
    data: reactions,
  };
};

/**
 * Calls data functions to get reactions of a comment
 * @param {Object} reactionsData functions that send queries to the database
 * @return {Object} { error if exists, reactions information if no error }
 */
export const getCommentReactions = (reactionsData) => async (userId, postId, commentId) => {
  const comment = await getCommentById(commentId);
  const post = await getPostById(postId);
  if (!post || !comment) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  const reactions = await reactionsData.getCommentReactionsByCommentId(commentId);
  if (!reactions) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: reactions,
  };
};

/**
 * Calls data functions to make a new reaction of a comment
 * @param {Object} reactionsData functions that send queries to the database
 * @return {Object} { error if exists, reactions information if no error }
 */
export const upsertCommentReaction = (reactionsData) => async (userId, postId, commentId, type) => {
  const comment = await getCommentById(commentId);
  const post = await getPostById(postId);
  // if a post or comment with that id does not exist, return an error
  if (!post || !comment) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  } else if (!(Object.values(reactionTypes).includes(type))) {
    return {
      error: errors.INVALID_REQUEST,
      data: null,
    };
  } else if (post.is_locked === 1) {
    return {
      error: errors.POST_LOCKED,
      data: null,
    };
  }

  const reaction = await reactionsData.getCommentReactionByIds(userId, commentId);

  // create comment reaction if it doesn't exist
  if (reaction === null) {
    await reactionsData.insertCommentReaction(userId, commentId, type);
    const reactions = await reactionsData.getCommentReactionsByCommentId(commentId);
    return {
      error: null,
      data: reactions,
    };
  }
  if (reaction.type === type) {
    const remainingReactions = await reactionsData.deleteCommentReaction(userId, commentId);
    return {
      error: null,
      data: remainingReactions,
    };
  }
  // update comment reaction if it already exists;
  await reactionsData.updateCommentReaction(userId, commentId, type);
  const reactions = await reactionsData.getCommentReactionsByCommentId(commentId);
  return {
    error: null,
    data: reactions,
  };
};
