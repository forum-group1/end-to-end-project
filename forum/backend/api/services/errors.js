export default {
  /** Such a record does not exist (when it is expected to exist) */
  NOT_FOUND: 1,
  /** The requirements do not allow such an operation */
  OPERATION_NOT_ALLOWED: 2,
  /** The requirements do not allow more than one of that resource */
  DUPLICATE_RECORD: 3,
  /** There is a problem in the request body or parameters */
  INVALID_REQUEST: 4,
  /** username/password mismatch */
  INVALID_SIGNIN: 5,
  /** user does not have action permission */
  UNAUTHORIZED: 6,
  /** user is banned and cannot do the current action */
  USER_BANNED: 7,
  /** post is locked and cannot be updated, deleted, reacted on, commented */
  POST_LOCKED: 8,
};
