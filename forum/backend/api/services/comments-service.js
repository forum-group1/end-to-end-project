/* eslint-disable max-len */
import errors from './errors.js';
import role from '../common/user-role.js';
import { getPostById } from '../data/posts-data.js';
import { getUserById } from '../data/users-data.js';
import { verifyPostLock } from './posts-service.js';


const isPostDeleted = async (postId) => {
  const currentPost = await getPostById(postId);
  // do something if result is null, as you cannot get is_deleted of undefined!
  if (currentPost.is_deleted === '1') {
    return true;
  }
  return false;
};

/**
 * Calls data functions to get all comments by post id
 * @param {Object} commentsData functions that send queries to the database
 * @return {Object} { error if exists, comments information if no error}
 */
export const getAllCommentsByPost = (commentsData) => async (postId) => {
  const isDeleted = await isPostDeleted(postId);
  if (isDeleted) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  const result = await commentsData.getAllCommentsByPost(postId);
  return {
    error: null,
    data: result,
  };
};

/**
 * Calls data functions to create a comment
 * @param {Object} commentsData functions that send queries to the database
 * @return {Object} { error if exists, comment information if no error}
 */
export const createComment = (commentsData) => async (comment, userId, postId, userRole) => {
  const isDeleted = await isPostDeleted(postId);
  if (isDeleted) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  const currentPost = await getPostById(postId);
  const isLocked = verifyPostLock(currentPost.is_locked);
  if (isLocked && userRole !== role.ADMIN) {
    return {
      error: errors.POST_LOCKED,
      data: null,
    };
  }
  const createdComment = await commentsData.createComment(comment, userId, postId);
  return {
    error: null,
    data: createdComment,
  };
};

/**
 * Calls data functions to update a comment
 * @param {Object} commentsData functions that send queries to the database
 * @return {Object} { error if exists, comment information if no error}
 */
export const updateComment = (commentsData) => async (postId, commentId, userId, userRole, comment) => {
  const isDeleted = await isPostDeleted(postId);
  if (isDeleted) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // if a post with that id does not exist, return an error
  const currentPost = await getPostById(postId);
  const isLocked = verifyPostLock(currentPost.is_locked);
  if (isLocked && userRole !== role.ADMIN) {
    return {
      error: errors.POST_LOCKED,
      data: null,
    };
  }
  if (currentPost === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // if a comment with that id does not exist, return an error
  const currentComment = await commentsData.getCommentById(commentId);
  if (currentComment === null || currentComment.is_deleted === 1) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // obtain user data to check for update privilleges
  const currentUser = await getUserById(userId);
  if (currentComment.users_id !== userId && currentUser.role !== role.ADMIN) {
    return {
      error: errors.UNAUTHORIZED,
      data: null,
    };
  }

  // update the comment values from the ones in the request body
  for (const key in comment) {
    if (comment.hasOwnProperty(key)) {
      currentComment[key] = comment[key];
    }
  }
  // send the updated Comment to the query
  const updatedComment = await commentsData.updateComment(commentId, currentComment);
  return {
    error: null,
    data: updatedComment,
  };
};

/**
 * Calls data functions to delete a comment
 * @param {Object} commentsData functions that send queries to the database
 * @return {Object} { error if exists, comment information if no error}
 */
export const deleteComment = (commentsData) => async (postId, commentId, userId, userRole) => {
  const isDeleted = await isPostDeleted(postId);
  if (isDeleted) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // if a post with that id does not exist, return an error
  const currentPost = await getPostById(postId);
  const isLocked = verifyPostLock(currentPost.is_locked);
  if (isLocked && userRole !== role.ADMIN) {
    return {
      error: errors.POST_LOCKED,
      data: null,
    };
  }
  if (currentPost === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // if a comment with that id does not exist, return an error
  const currentComment = await commentsData.getCommentById(commentId);
  if (currentComment === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // obtain user data to check for update privilleges
  const currentUser = await getUserById(userId);
  if (currentComment.users_id !== userId && currentUser.role !== role.ADMIN) {
    return {
      error: errors.UNAUTHORIZED,
      data: null,
    };
  }
  // send the CommentId to the query to mark the is_deleted field as true
  const remainingComments = await commentsData.deleteComment(postId, commentId);
  return {
    error: null,
    data: remainingComments,
  };
};
