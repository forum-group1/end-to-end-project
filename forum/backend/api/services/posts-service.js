/* eslint-disable max-len */
/* eslint-disable consistent-return */
import { getUserById } from '../data/users-data.js';
import role from '../common/user-role.js';
import errors from './errors.js';


export const verifyPostLock = (isLocked) => {
  if (isLocked === 1) {
    return true;
  }
  return false;
};

/**
 * Calls data functions to get post information by post id
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, post information if no error }
 */
export const getPostById = (postsData) => async (postId) => {
  const post = await postsData.getPostById(postId);
  if (post === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: post,
  };
};

/**
 * Calls data functions to get post information by username
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, post information if no error }
 */
export const getPostsByUsername = (postsData) => async (username) => {
  const posts = await postsData.getPostsByUsername(username);
  if (!posts) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: posts,
  };
};

/**
 * Calls data functions to get all posts
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, posts information if no error}
 */
export const getAllPosts = (postsData) => async () => {
  const allPosts = await postsData.getAllPosts();
  if (!allPosts) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allPosts,
  };
};

/**
 * Calls data functions to get post information by post id
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, post information if no error}
 */
export const getFollowersPosts = (postsData) => async (userId) => {
  const followersPosts = await postsData.getFollowersPosts(userId);
  if (!followersPosts) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: followersPosts,
  };
};

/**
 * Calls data functions tocreate a new post
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, post information if no error}
 */
export const createPost = (postsData) => async (post) => {
  const createdPost = await postsData.createPost(post);
  return {
    error: null,
    data: createdPost,
  };
};

/**
 * Calls data functions to update post information by post id
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, post information if no error}
 */
export const updatePost = (postsData) => async (postId, userId, userRole, post) => {
  const currentPost = await postsData.getPostById(postId);
  // if a post with that id does not exist, return an error
  if (currentPost === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  const isLocked = verifyPostLock(currentPost.is_locked);
  if (isLocked && userRole !== role.ADMIN) {
    return {
      error: errors.POST_LOCKED,
      data: null,
    };
  }


  // send the updated post to the query
  const updatedPost = await postsData.updatePost(postId, post);
  if (updatedPost === null) {
    return {
      error: errors.INVALID_REQUEST,
      data: null,
    };
  }
  return {
    error: null,
    data: updatedPost,
  };
};

/**
 * Calls data functions to delete post by post id
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, post information if no error}
 */
export const deletePost = (postsData) => async (postId, userId, username) => {
  // if a post with that id does not exist, return an error
  if (!(await postsData.postExists(postId))) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // obtain user data to check for update privilleges
  const currentPost = await postsData.getPostById(postId);
  const currentUser = await getUserById(userId);
  if (currentPost.users_id !== userId && currentUser.role !== role.ADMIN) {
    return {
      error: errors.UNAUTHORIZED,
      data: null,
    };
  }
  // send the postId to the query to mark the is_deleted field as true
  const allPosts = await postsData.deletePost(postId);
  const userPosts = await postsData.getPostsByUsername(username);
  const followersPosts = await postsData.getFollowersPosts(userId);
  const singlePost = await postsData.getPostById(postId);

  return {
    error: null,
    data: { allPosts: allPosts, userPosts: userPosts, followersPosts: followersPosts, singlePost: singlePost },
  };
};

/**
 * Calls data functions to flag post by post id
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, post information if no error}
 */
export const flagPost = (postsData) => async (postId, userId, userRole, username) => {
  // if a post with that id does not exist, return an error
  if (!(await postsData.postExists(postId))) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  const curPost = await postsData.getPostById(postId);
  const isLocked = verifyPostLock(curPost.is_locked);
  if (isLocked && userRole !== role.ADMIN) {
    return {
      error: errors.POST_LOCKED,
      data: null,
    };
  }
  // if (curPost.is_flagged === 1) {
  //   return {
  //     data: await postsData.getAllPosts(),
  //     error: null,
  //   };
  // }

  await postsData.flagPost(postId, userId);

  const allPosts = await postsData.getAllPosts();
  const userPosts = await postsData.getPostsByUsername(username);
  const followersPosts = await postsData.getFollowersPosts(userId);
  const singlePost = await postsData.getPostById(postId);
  if (allPosts === null) {
    return {
      error: errors.INVALID_REQUEST,
      data: null,
    };
  }
  return {
    error: null,
    data: { allPosts: allPosts, userPosts: userPosts, followersPosts: followersPosts, singlePost: singlePost },
  };
};

/**
 * Calls data functions to lock post by post id
 * @param {Object} postsData functions that send queries to the database
 * @return {Object} { error if exists, post information if no error}
 */
export const lockPost = (postsData) => async (postId, userId, userRole) => {
  // if a post with that id does not exist, return an error
  if (!(await postsData.postExists(postId))) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  const curPost = await postsData.getPostById(postId);
  const isLocked = verifyPostLock(curPost.is_locked);
  if (isLocked && userRole !== role.ADMIN) {
    return {
      error: errors.POST_LOCKED,
      data: null,
    };
  }
  if (curPost.users_id !== userId && userRole !== role.ADMIN) {
    return {
      error: errors.UNAUTHORIZED,
      data: null,
    };
  }

  await postsData.lockPost(postId);
  const currentPost = await postsData.getPostById(postId);
  return {
    data: currentPost,
    error: null,
  };
};
