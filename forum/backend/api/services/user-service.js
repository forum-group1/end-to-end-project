/* eslint-disable max-len */
import bcrypt from 'bcrypt';
import { DEFAULT_BAN_DURATION } from '../common/admin-constants.js';
import userRole from '../common/user-role.js';
import errors from './errors.js';

/**
 * Calls data functions to validate the user's information for logging in
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const loginUser = (usersData) => async (username, password) => {
  const user = await usersData.getUserByUsername(username);

  // check if user exists and return error if not
  if (!(user)) {
    return {
      error: errors.NOT_FOUND,
      user: null,
    };
  //   // check if the user is banned
  // } else if ((user.banned_until !== null) && (user.banned_until > new Date())) {
  //   return {
  //     error: errors.USER_BANNED,
  //     user: null,
  //   };
    // check if the password from the request matches the one in the system
  } else if (!(await bcrypt.compare(password, user.password))) {
    return {
      error: errors.INVALID_SIGNIN,
      user: null,
    };
  }
  // if all the tests pass, return the user's data
  return {
    error: null,
    user: user,
  };
};


/**
 * Calls data functions to get user's information by username
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const getUserByUsername = (usersData) => async (username) => {
  const user = await usersData.getUserByUsername(username);
  // if user with that username does not exist, return an error
  if (user === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // if user with that username is found, return them
  return {
    error: null,
    data: user,
  };
};

/**
 * Calls data functions to get user's information by id
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const getUserById = (usersData) => async (userId) => {
  const user = await usersData.getUserById(userId);
  // if user with that username does not exist, return an error
  if (user === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // if user with that username is found, return them
  return {
    error: null,
    data: user,
  };
};

/**
 * Calls data functions to get all users's information
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, users' data if no error }
 */
export const getAllUsers = (usersData) => async () => {
  const allUsers = await usersData.getAllUsers();
  if (allUsers === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allUsers,
  };
};

/**
 * Calls data functions to create a new user
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const createUser = (usersData) => async (user) => {
  // check if user already exists and return error if it does
  if (await usersData.userExists(user.username)) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  // hash the password in 10 rounds of calculations
  const hashedPassword = await bcrypt.hash(user.password, 10);
  // create a default user with the new hashed password and return them
  const createdUser = await usersData.createUser(user, hashedPassword, userRole.USER);
  if (createdUser.id) {
    return {
      error: null,
      data: createdUser,
    };
  }
  return {
    error: errors.INVALID_REQUEST,
    data: null,
  };
};

/**
 * Calls data functions to delete a user by username
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const deleteUser = (usersData) => async (username) => {
  const deletedUser = await usersData.deleteUser(username);

  // if no users were affected because the username doesn't exist
  if (deletedUser === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // return the username of the deleted individual
  return {
    error: null,
    data: deletedUser,
  };
};

/**
 * Calls data functions to ban a user by username
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const banUser = (usersData) => async (username) => {
  const bannedUntilDate = new Date();
  bannedUntilDate.setMilliseconds(bannedUntilDate.getMilliseconds() + DEFAULT_BAN_DURATION);

  const bannedUser = await usersData.banUser(username, bannedUntilDate);

  // if no users were affected because the username doesn't exist
  if (bannedUser === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  // return the username of the banned individual
  return {
    error: null,
    data: bannedUser,
  };
};

/**
 * Calls data functions to follow a user by id
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const addFriend = (usersData) => async (userId, friendId) => {
  const friendAdded = await usersData.addFriend(userId, friendId);
  if (friendAdded === null) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  return {
    error: null,
    data: friendAdded,
  };
};

/**
 * Calls data functions to unfollow a user by id
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const deleteFriend = (usersData) => async (userId, friendId) => {
  const friendDeleted = await usersData.deleteFriend(userId, friendId);
  if (!friendDeleted) {
    return {
      error: errors.INVALID_REQUEST,
      data: null,
    };
  }

  return {
    error: null,
    data: friendDeleted,
  };
};

/**
 * Calls data functions to get all followees
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const getAllFriends = (usersData) => async (userId) => {
  const allFriends = await usersData.getAllFriends(userId);
  if (!allFriends) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allFriends,
  };
};

/**
 * Calls data functions to determine if a users is followed or not
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */
export const findFriendship = (usersData) => async (userId, friendId) => {
  const isFriend = await usersData.findFriendship(userId, friendId);
  return {
    error: null,
    data: isFriend,
  };
};
